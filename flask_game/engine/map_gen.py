import pickle
import numpy as np
import pandas as pd
import os
import sklearn.preprocessing as preprocessing
from scipy import signal
from flask_game.engine import conf, db_engine

def generate_map(game_name, input_x_y, smooth_size1, smooth_size2, filter_method):
    nx = input_x_y
    ny = input_x_y

    dem1 = np.random.rand(nx,ny)
    #dem1 = np.random.random((nx, ny)) - 50
    #dem1 = np.random.randint(32000, size=(nx, ny))

    #try either of these, with more sqrt or products if necessary
    dem1 = np.sqrt(dem1)
    #dem1 = dem1*dem1

    sizex = smooth_size1
    sizey = smooth_size1

    x, y = np.mgrid[-sizex:sizex+1, -sizey:sizey+1]
    g = np.exp(-0.333*(x**2/float(sizex*12)+y**2/float(sizey*14)))
    if not 'filter_method' in locals():
        filter_method = 1

    if filter_method == 1:
        filter = g
    elif filter_method == 2:
        filter = (g/20000)/(g.sum() ** 2)
    elif filter_method == 3:
        filter = g/g.sum() 
    elif filter_method == 4:
        filter = g/(g ** g)

    demSmooth = signal.convolve(dem1, filter, mode='valid')
    demSmooth = (demSmooth - demSmooth.min())/(demSmooth.max() - demSmooth.min())
    #demSmooth = demSmooth/demSmooth.min()

    z_scaler = preprocessing.MinMaxScaler()
    z_data = z_scaler.fit_transform(demSmooth)

    z_data = [[x if x >= 0.55 else 0 for x in line] for line in z_data]
    z_data = np.around(z_data, decimals=2)

    sizex = smooth_size2
    sizey = smooth_size2

    x, y = np.mgrid[-sizex:sizex+1, -sizey:sizey+1]
    g = np.exp(-0.333*(x**2/float(sizex*12)+y**2/float(sizey*14)))

    if filter_method == 1:
        filter = g
    elif filter_method == 2:
        filter = (g/20000)/(g.sum() ** 2)
    elif filter_method == 3:
        filter = g/g.sum() 
    elif filter_method == 4:
        filter = g/(g ** g)

    smoothy = signal.convolve(z_data, filter, mode='valid')
    smoothy = (smoothy - smoothy.min())/(smoothy.max() - smoothy.min())
    smoothy = np.around(smoothy, decimals=2)
    act_x = len(smoothy)
    act_y = len(smoothy[0])

    steps = np.linspace(0, 0.99, 101)
    alt_list = [-10000,-9630,-9260,-8890,-8520,-8150,-7780,-7410,-7040,-6670,-6300,-5930,-5560,
            -5190,-4820,-4450,-4080,-3710,-3340,-2970,-2600,-2230,-1860,-1490,-1120,-750,-380,
            -10,0,10,200,318,436,554,672,790,908,1026,1144,1262,1380,1498,1616,1734,1852,1970,
            2088,2206,2324,2442,2560,2678,2796,2914,3032,3150,3268,3386,3504,3622,3740,3858,3976,
            4094,4212,4330,4448,4566,4684,4802,4920,5038,5556,6274,6859,7477,8095,8713,9331,9949,
            10567,11685,12803,13921,15039,16157,17275,18393,19511,20629,21747,22865,23983,25101,
            26219,27337,28455,29573,30691,31809,32927]
    steps = np.around(steps, decimals=2)
    the_numbaz = list(zip(alt_list,steps))

    df_alt_range = pd.DataFrame(the_numbaz)
    df_alt_range = df_alt_range.rename(index=str, columns={0: "elevation", 1: "z"})


    z_list = [z for sub_z in smoothy for z in sub_z]
    x_list = []
    y_list= []
    for y in range(len(smoothy)):
        for x in range(len(smoothy[0])):
            x_list.append(x)
            y_list.append(y)
    both_lists = list(zip(x_list, y_list, z_list))

    df_new = pd.DataFrame(both_lists, columns=['x','y','z'])
    df_biomes = pd.read_sql('select * from biomes', db_engine)

    #df_xyz = pd.merge(df_new, df_alt_range, on='z', how='inner')
    equator = round(df_new['y'].mean())
    df_new['equator_distance'] = df_new.apply(distance_to_equator, axis=1, args=(equator,))
    df_new['biomeid_a'] = df_new.apply(calculate_biome, axis=1, args=(equator, df_biomes,))
    df_new['elevation'] = 0
    pickle.dump(df_new, open(f'{os.path.join(conf.PICKLE_PATH, game_name)}.p', 'wb'))
    return act_x, act_y

def distance_to_equator(row, equator):
    if row['y'] < equator:
        dist_to_equator = equator - row['y']
    else:
        dist_to_equator = row['y'] - equator   
    return dist_to_equator

def calculate_biome(row, equator, df_biomes):

    ice_zone = equator - round(equator/10)
    cold_zone = equator - round(equator/7)
    taiga_zone = equator - round(equator/4)
    forest_zone = equator - round(equator/1.9)
    tropic_zone = equator - round(equator/1.4)
    savanna_zone = equator - round(equator/1.1)

    if row['z'] < 0.3:
        biomeid_a = 50
    elif row['equator_distance'] >= ice_zone:
        biomeid_a = df_biomes.id[df_biomes.biome_name == 'Polar'].item()

    elif row['equator_distance'] >= cold_zone:
        if row['z'] <= 0.6:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Tundra'].item()
        elif row['z'] <= 0.8:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Boreal Forests'].item()
        else:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Mountain'].item()

    elif row['equator_distance'] >= taiga_zone:
        if row['z'] <= 0.6:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Mediterranean Forests'].item()
        elif row['z'] <= 0.8:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Boreal Forests'].item()
        else:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Mountain'].item()

    elif row['equator_distance'] >= forest_zone:
        if row['z'] <= 0.6:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Temperate Grasslands'].item()
        elif row['z'] <= 0.8:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Mediterranean Forests'].item()
        else:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Mountain'].item()

    elif row['equator_distance'] >= tropic_zone:
        if row['z'] <= 0.6:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Temperate Broadleaf'].item()
        elif row['z'] <= 0.8:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Temperate Conifer Forests'].item()
        else:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Mountain'].item() 

    elif row['equator_distance'] >= savanna_zone:
        if row['z'] <= 0.6:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Tropical Moist Forests'].item()
        elif row['z'] <= 0.8:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Montane'].item()
        else:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Mountain'].item()
    
    elif row['equator_distance'] <= savanna_zone:
        if row['z'] <= 0.6:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Tropical Grasslands'].item()
        elif row['z'] <= 0.8:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Montane'].item()
        else:
            biomeid_a = df_biomes.id[df_biomes.biome_name == 'Mountain'].item()


    return biomeid_a

def calculate_mountains(row):
    if row['elevation'] > 23000:
        biomeid_b = '1'
    elif row['elevation'] < 0:
        biomeid_b = '2'
    else:
        biomeid_b = '0'
        #Mangroves, deltas, swamps, lakes n shit maybe added here?
    return biomeid_b

def store_map(game_name):
    df_xyz = pickle.load(open(f'{os.path.join(conf.PICKLE_PATH, game_name)}.p', 'rb'))
    df_map_id = pd.read_sql(f"select id from game_maps where game_name = '{game_name}'", db_engine)
    df_xyz['mapid'] = df_map_id.id.item()
    df_xyz.to_sql('game_map_data', db_engine, if_exists='append', index=False)
    df_xyz = pd.read_sql(f'''
        select gmd.x, gmd.y, gmd.z, gmd.elevation, gb.biome_name
        from game_map_data as gmd
        inner join biomes as gb on gmd.biomeid_a = gb.id
        inner join game_maps as gm on gmd.mapid = gm.id
        where gm.game_name = '{game_name}'
        order by gmd.x desc, gmd.y
        ''', db_engine)
    pickle.dump(df_xyz, open(f'{os.path.join(conf.PICKLE_PATH, game_name)}.p', 'wb'))
#SET BIOME ZONES...

#df_xyz['biomeid_b'] = df_xyz.apply(calculate_mountains, axis=1)
    