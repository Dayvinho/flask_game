go
use TERRARUM

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_maps')
begin
drop table dbo.game_maps
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'users_tbl')
begin
drop table dbo.users_tbl
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'site_fp_posts')
begin
drop table dbo.site_fp_posts
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'months')
begin
drop table dbo.months
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'dieases')
begin
drop table dbo.dieases
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'genders')
begin
drop table dbo.genders
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'sexual_orientaton')
begin
drop table dbo.sexual_orientaton
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'interactions')
begin
drop table dbo.interactions
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'body_parts')
begin
drop table dbo.body_parts
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'addictions')
begin
drop table dbo.addictions
end

go
begin
create table game_maps (
id int identity(1,1) primary key,
game_name nvarchar(50) not null unique,
input_x int not null,
input_y int not null,
act_x int,
act_y int,
datecreated datetime not null default(getdate()),
)
end

go
begin
create table users_tbl (
id int identity(1,1) primary key,
username nvarchar(20) unique not null,
email nvarchar(120) not null,
image_file nvarchar(20) not null default 'default.jpg',
passw nvarchar(60) not null,
gameid int, --soft FK to game_maps.id for game they are currently playing
datecreated datetime not null default(getdate()),
)
end

go
begin
create table site_fp_posts (
id int identity(1,1) primary key,
userid int not null,
title nvarchar(100) not null,
post nvarchar(max) not null,
datecreated datetime not null default(getdate()),
constraint FK_FPP_UID FOREIGN KEY (userid) REFERENCES users_tbl(id), 
)
end

go
BEGIN
create table months (
monthid int primary key,
month_name nvarchar(20) unique not null,    
)
end

go
BEGIN
create table addictions (
id int identity(1,1) primary key,
addiction nvarchar(50) unique not null,
prod_impact float not null, --productivity impact
pot_violence float not null, --potential violence
pot_insanity float not null, --potential insanity
pot_rehab float not null, --potential of rehabilitation
)
end

go
BEGIN
create table diseases (
id int identity(1,1) primary key,
disease nvarchar(50) unique not null,
disease_desc nvarchar(max),
can_function bit not null, --can the peon do anything?
prod_impact float not null, --productivity impact
vomit bit not null, --voimit yes no
puss bit not null, --puss yes no
seizure bit not null,
fever bit not null,
lust_impact float not null, --impact to peons lust
att_impact int not null, --peon attractiveness impact
pot_insanity float not null, --potential of insanity
pot_treatment float not null, --potential of treatment
pot_death float not null, --potential of death
)
end

go
begin
create table genders (
id int identity(1,1) primary key,
gender nvarchar(20) unique not null,
)
end

go
begin
create table sexual_orientaton (
id int identity(1,1) primary key,
orientation nvarchar(50) unique not null,
)
end

GO
BEGIN
CREATE table interactions (
id int identity(1,1) primary key,
interaction nvarchar(100) unique not null,
)
end



go
begin
create table body_parts (
id int identity(1,1) primary key,
limb_name nvarchar(30) unique not null,
death_chance float not null,
prod_impact float not null, -- productivity impact if limb is missing
att_impact int not null, --attractiveness impact
str_impact int not null, --strength
int_impact int not null, --intelligence
cha_impact int not null, --charisma 
req nvarchar(30) not null, --required body part
)
end

GO
begin
create table gen_status (
id int identity(1,1) primary key,
gen_status nvarchar(30) unique not null,
)
end

go
BEGIN
create table events (
id int identity(1,1) primary key,
event_desc nvarchar(max)
)
END

GO
BEGIN
create table quests (
id int identity(1,1) primary key,
quest_desc nvarchar(max)
)
end