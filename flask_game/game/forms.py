from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, DecimalField, SelectField
from wtforms.validators import DataRequired, Length, ValidationError, NumberRange
from flask_game.models import game_maps
from flask_game.engine import conf, db_engine
import pandas as pd
import sys

class CreateNewGame(FlaskForm):
    game_name = StringField('Enter Game Name',
                           validators=[DataRequired(), Length(min=5, max=30)])
    input_x_y = IntegerField('X & Y Value',
                           validators=[DataRequired(), 
                           NumberRange(min=50, max=500, message='Please Entera number: 50-500')])
    smooth_x_y1 = IntegerField('Smoothing Size',
                           validators=[DataRequired(), 
                           NumberRange(min=2, max=20, message='Please Entera number: 2-20')])
    smooth_x_y2 = IntegerField('Smoothing Size',
                           validators=[DataRequired(), 
                           NumberRange(min=2, max=20, message='Please Entera number: 2-20')])
    filter_method = IntegerField('Filter Method',
                           validators=[DataRequired(), 
                           NumberRange(min=1, max=4, message='Please Entera number: 1-4')])
    submit = SubmitField('Generate Map!')

    def validate_username(self, game_name):
        game_check = game_maps.query.filter_by(game_name=game_name.data).first()
        if game_check:
            raise ValidationError('This game name is taken. Please choose a different one.')

class ConfirmNewGame(FlaskForm):
    submit_confirm = SubmitField('Confirm')

class CancelNewGame(FlaskForm):
    submit_cancel = SubmitField('Cancel')

class PickStartLocation(FlaskForm):
    x_position = IntegerField('X Value')
    y_position = IntegerField('Y Value')
    race = SelectField('Race', coerce=int)
    submit = SubmitField('Confirm Start Location!')

    def __init__(self, game_name, *args, **kwargs):
        super(PickStartLocation, self).__init__(*args, **kwargs)
        races = pd.read_sql('''
            select id, race_name from races
            where race_name = 'Men'
            ''', db_engine)
            # adding more races will come way later...
        game_check = game_maps.query.filter_by(game_name=game_name).first()
        self.x_position.validators = [DataRequired(), NumberRange(min=0, max=game_check.act_x, 
                                        message='Please enter your desired X co-ord')]
        self.y_position.validators = [DataRequired(), NumberRange(min=0, max=game_check.act_y, 
                                        message='Please enter your desired Y co-ord')]
        self.race.choices = [(raceid, race) for raceid, race in list(zip(races['id'], races['race_name']))]

class JoinGame(FlaskForm):
    game = SelectField('Games', coerce=int)
    submit = SubmitField('Join Game')

    def __init__(self, *args, **kwargs):
        super(JoinGame, self).__init__(*args, **kwargs)
        games = pd.read_sql(f"select id, game_name from game_maps", db_engine)
        self.game.choices = [(gameid, game_name) for gameid, game_name in list(zip(games['id'], games['game_name']))]