import sys
import pandas as pd
import numpy as np
import pickle
import os
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, plot
from plotly import offline
from flask import render_template, request, Blueprint, url_for, redirect, Markup, flash
from flask_login import login_required, current_user
from flask_game.game.forms import CreateNewGame, PickStartLocation, JoinGame
from flask_game.engine.map_gen import generate_map
from flask_game.engine.peon_gen import (generate_family_sizes, generate_family_members, 
                                        generate_interactions)
from flask_game.engine.res_gen import generate_resources
from flask_game.engine.species_gen import generate_species
from flask_game.engine.buildings import (add_start_buildings, refresh_building_data,
                                        farm_produce_select)
from flask_game.engine.resource imoprt add_start_resources
from flask_game.engine import conf, db_engine
from pathlib import Path

create_game = Blueprint('create_game', __name__)

@create_game.route("/create_new_game", methods=['POST', 'GET'])
@login_required
def create_new_game():
    form = CreateNewGame()
    if form.validate_on_submit():
        game_name = form.game_name.data
        input_x_y = form.input_x_y.data
        smooth_size1 = form.smooth_x_y1.data
        smooth_size2 = form.smooth_x_y1.data
        filter_method = form.filter_method.data
        act_x, act_y = generate_map(game_name, input_x_y, smooth_size1, 
                                    smooth_size2, filter_method)
        insert_data = {
            'game_name': game_name,
            'input_x': input_x_y,
            'input_y': input_x_y,
            'act_x': act_x,
            'act_y': act_y
        }

        df_map_insert = pd.DataFrame(insert_data, index=[0])
        df_map_insert.to_sql('game_maps', db_engine, if_exists='append', index=False)
        return redirect(url_for('game_map.game_map_preview', game_name=game_name))
    return render_template('create_game.html', title='Create New Game', form=form)

@create_game.route("/pick_start_location", methods=['POST', 'GET'])
@login_required
def pick_start_location():
    game_name = request.args.get('game_name')
    userid = request.args.get('userid')
    df_mapid = pd.read_sql(f"""
        select id from game_maps where game_name = '{game_name}'
        """, db_engine)
    mapid = df_mapid.id.item()
    print(type(current_user))

    form = PickStartLocation(game_name)
    if request.method == 'GET':
        path = os.path.join(conf.PICKLE_PATH, f'contour_{game_name}.p')
        check_path = Path(path)
        if check_path.is_file():
            contour = pickle.load(open(os.path.join(conf.PICKLE_PATH, 
                                        f'contour_{game_name}.p'), 'rb'))
        else:
            df_xyz = pickle.load(open(f'{os.path.join(conf.PICKLE_PATH, game_name)}.p', 'rb'))
            z_list = df_xyz['z'].tolist()
            x_list = df_xyz['x'].tolist()
            y_list = df_xyz['y'].tolist()
            
            ocean_max = df_xyz.loc[df_xyz['biome_name'] == 'Ocean', 'z'].max()

            graph_text = df_xyz['biome_name'].tolist()
            contour = go.Contour(z=z_list, x=x_list, y=y_list, text=graph_text, 
            #data = [go.Contour(z=z_repack, text=graph_text, 
                                colorscale=[
                [0.0, 'rgb(1, 1, 129)'],[0.2, 'rgb(1, 1, 129)'],
                [0.2, 'rgb(6, 4, 209)'],[ocean_max, 'rgb(6, 4, 209)'],
                [ocean_max, 'rgb(1, 1, 175)'],[0.3, 'rgb(1, 1, 175)'],
                [0.3, 'rgb(1, 131, 0)'],[0.4, 'rgb(1, 131, 0)'],
                [0.4, 'rgb(96, 158, 0)'],[0.5, 'rgb(96, 158, 0)'],
                [0.5, 'rgb(2, 113, 0)'],[0.6, 'rgb(2, 113, 0)'],
                [0.6, 'rgb(110, 113, 0)'],[0.7, 'rgb(110, 113, 0)'],
                [0.7, 'rgb(140, 140, 140)'],[0.8, 'rgb(140, 140, 140)'],
                [0.8, 'rgb(160, 160, 160)'],[0.9, 'rgb(160, 160, 160)'],
                [0.9, 'rgb(255, 255, 232)'],[1.0, 'rgb(255, 255, 232)']])
            pickle.dump(contour, open(os.path.join(conf.PICKLE_PATH, 
                                    f'contour_{game_name}.p'), 'wb'))

        player_markers = pd.read_sql(f"""
            select gmd.x, gmd.y, u.username from game_map_tile_owner as gmto
            inner join game_map_data as gmd on gmto.tileid = gmd.id
            inner join users_tbl as u on gmto.userid = u.id
            where mapid = {mapid}
            """, db_engine)

        # continue from pickle load
        data = [contour]
        print('3')
        markers_list = []
        for _, row in player_markers.iterrows():
            marker = dict(x=row['x'],
                y=row['y'],
                xref='x',
                yref='y',
                text=row['username'],
                font=dict(color='#d10429'),
                showarrow=True,
                align='center',
                arrowhead=2,
                arrowsize=1,
                arrowwidth=2,
                arrowcolor='#636363',
                ax=20,
                ay=-30,
                bordercolor='#c7c7c7',
                borderwidth=2,
                borderpad=4,
                bgcolor='#ff7f0e',
                opacity=0.8)
            markers_list.append(marker)

        layout = go.Layout(autosize=False, width=1250, height=1250,
                            margin=go.layout.Margin(l=50,r=50,b=100,t=100,pad=4),
                            paper_bgcolor='#303030', 
                            plot_bgcolor='#303030',
                            font=dict(color='#ffffff', size=14),
                            title='Pick Start Location',
                            showlegend=False,
                            annotations=markers_list
                            )
        print('4')
        fig = go.Figure(data=data, layout=layout)
        print('5')
        div = offline.plot(fig, show_link=False, output_type="div")
        div = Markup(div)
        #with open('test_file.txt', 'w') as f:
        #    f.write(div)
        print('6')

    elif request.method == 'POST':
        
        if form.validate_on_submit():
            raceid = form.race.data

            user_check = pd.read_sql(f"""
                select id from game_maps_users where mapid = {mapid} 
                and userid = {userid}
                """, db_engine)
            if user_check.empty == False:
                flash(f'User is already playing this game!', 'warning')
                return redirect(url_for('game_routes.overview'))

            check_xy = pd.read_sql(f'''
                select id from game_maps_users where 
                start_x = {form.x_position.data} 
                and start_y = {form.y_position.data} and mapid = {mapid}
                ''', db_engine)

            if check_xy.empty == False:
                flash(f'This tile is already taken, please choose another!', 
                        'warning')
                return redirect(url_for('create_game.pick_start_location'))

            print('1')
            second_xy_check = pd.read_sql(f"""
                select id, biomeid_a from game_map_data where x = {form.x_position.data} 
                and y = {form.y_position.data} and mapid = {mapid}
                """, db_engine)
            biome_check = second_xy_check.biomeid_a.item()
            second_xy_check = second_xy_check.id.item()            

            xy_check = pd.read_sql(f"""
                select id from game_map_tile_owner where tileid = {second_xy_check}
                """, db_engine)

            if xy_check.empty == False:
                flash(f'This tile is already taken, please choose another!', 
                        'warning')
                return redirect(url_for('create_game.pick_start_location'))

            if biome_check == 50:
                flash(f'You Cannot Start Adrift At Sea You Imbecile', 
                        'warning')
                return redirect(url_for('create_game.pick_start_location'))

            insert_data = {
                'userid': userid,
                'mapid': mapid,
                'raceid': raceid,
                'start_x': form.x_position.data,
                'start_y': form.y_position.data
            }

            df_insert = pd.DataFrame(insert_data, index=[0])
            df_insert.to_sql('game_maps_users', db_engine, if_exists='append', 
                            index=False)
            print('2')
            tileid = pd.read_sql(f'''
            select id from game_map_data where mapid = {mapid} 
            and x = {form.x_position.data} and y = {form.y_position.data}
            ''', db_engine)
            tileid = tileid.id.item()

            insert_data2 = {
                'tileid': tileid,
                'userid': userid
            }
            df_insert2 = pd.DataFrame(insert_data2, index=[0])
            df_insert2.to_sql('game_map_tile_owner', db_engine, 
                            if_exists='append', index=False)

            #get these values from DB at some point, race specific
            start_peon_count = 1000
            low = 1
            high = 8
            
            generate_family_sizes(start_peon_count, low, high, userid, mapid)
            generate_family_members(userid, mapid)
            generate_interactions(userid, mapid)

            df_biomeid = pd.read_sql(f'''
                    select biomeid_a from game_map_data 
                    where id = {tileid}
                    ''', db_engine)
            biomeid = df_biomeid.biomeid_a.item()
            generate_resources(tileid, biomeid)
            generate_species(tileid, biomeid)

            df_building_data = refresh_building_data()
            add_start_buildings(tileid, df_building_data)
            farmid = df_building_data.id[df_building_data.b_name == 'Farm'].item()
            farm_gbids = pd.read_sql(f'''
                select id from game_map_tile_buildings 
                where bid = {farmid}
                and tileid = {tileid}
                ''', db_engine)
            produce_ids = pd.read_sql(f'''
                select gmtr.resid from game_map_tile_res as gmtr
                inner join resources as r on gmtr.resid = r.id
                where tileid = {tileid}
                and r.res_typeid in (10,11,12)
                ''', db_engine)

            produce_ids = produce_ids.resid.to_list()
            farm_gbids = farm_gbids.id.to_list()
            farm_produce_select(farm_gbids, produce_ids, 'resources')

            # Starting Resources
            fid = np.random.choice(produce_ids)
            start_res = [53, 59, fid]
            amounts = [1000, 1000, 3500]

            for res, amount in zip(start_res, amounts):
                add_start_resources(userid, mapid, res, amount)
            
            return redirect(url_for('game_routes.overview'))

    return render_template('pick_start_location.html', 
                        title='Pick Start Location', 
                        div=div,
                        form=form,
                        game_name=game_name)





