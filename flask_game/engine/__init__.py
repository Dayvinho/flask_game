import sys
from sqlalchemy import create_engine
from os.path import abspath, join, realpath
from os import pardir

root_path = abspath(join(realpath(__file__), pardir))
root_path = abspath(join(root_path, pardir))
sys.path.append(root_path)
from flask_game import config

conf = config.Config()
db_engine = create_engine(conf.SQLALCHEMY_DATABASE_URI)