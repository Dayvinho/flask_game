go
use TERRARUM

GO
begin
bulk insert building_types
from 'C:\FlaskGame\flask_game\flask_game\sql\building_types_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end

GO
begin
bulk insert buildings
from 'C:\FlaskGame\flask_game\flask_game\sql\buildings_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end