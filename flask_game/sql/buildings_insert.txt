id	btypeid	b_name	b_desc	b_points_required	size	capacity	storage	min_turns_to_build	max_workers_assigned	max_military_assigned	max_number_of_building	biomeid	parent_building
1	1	Small House	A small house for a family	500	25	6	5	1	0	0	0	99	0
2	1	Medium House	A medium sized house for a family	1250	60	10	12	1	0	0	0	99	0
3	1	Large House	A large house for a family	2500	140	14	28	1	0	0	0	99	0
4	1	Orphanage	A place for orphaned children to live	2500	135	25	27	1	2	0	0	99	0
5	1	Reeve	A Reeve acts as a head of community	5000	50	10	5	1	0	0	3	99	0
6	1	Priests Parsonage	A place for priests to live away from sin	5000	50	10	5	1	0	0	0	99	0
7	1	Knights Barracks	A place for heroes and knights to stay in safety	50000	50	10	5	1	0	0	0	99	0
8	1	Manor House	A place where upper class peons will live	25000	50	10	5	1	0	0	0	99	0
9	1	Almshouse	A house to accommodate the poorest of the poor	500	25	12	5	1	0	0	0	99	0
10	2	Saw Mill	Refine Wood into Wooden Planks	500	25	0	5	1	2	0	0	99	0
11	2	Forester	Chop down trees to produce wood	500	25	0	5	1	4	0	0	99	0
12	2	Reforestry Office	Regrow trees for sustainable wood production	500	25	0	5	1	2	0	0	99	0
13	3	Farm	A fruit or vegetable farm	5000	100000	24	20000	1	30	0	0	99	0
14	3	Livestock Farm	A livestock farm	5000	50000	12	10000	1	15	0	0	99	0
15	3	Vinyard	Where grapes are grown and wine is produced	25000	50000	12	10000	1	15	0	0	99	0
16	3	Slave Farm	"Where peons are born for one purpose, to be slaves"	150000	20000	2000	0	1	30	0	0	99	0
17	3	Huntsmans Cabin	A cabin used by huntsman before going to capture or kill beasts	5000	25	0	8	1	4	0	0	99	0
18	4	Mine	Mine stuff you find in the ground	5000	25000	24	5000	1	30	0	0	99	0
19	5	Quarry	Smash big stones into smaller ones	5000	50000	24	10000	1	30	0	0	99	0
20	6	Potter	The people need pots!	500	25	0	5	1	2	0	0	99	0
21	7	Small Church	A humble place for peons to pray and attend church survices	2500	50	0	5	1	1	0	0	99	0
22	7	Small Bell Tower	Ring bells to the heavans!	500	0	0	0	1	0	0	1	99	21
23	7	Monastery�	A large building of religious study and worhship	250000	450	50	50	1	0	0	5	99	0
24	7	Small Temple	A place to pray and pay homage to a god	2500	10	0	2	1	1	0	0	99	0
25	7	Medium Temple	A place to pray and pay homage to a god	50000	100	0	20	1	2	0	0	99	0
26	7	Large Temple	A place to pray and pay homage to a god	1000000	1000	0	200	1	5	0	0	99	0
27	7	Sanctuary	A�sanctuary�is a sacred space	5000000	1000	0	250	1	6	0	1	99	0
28	7	Sacrafical Pit	Where peons are sacraficed to the gods	15000	50	1	0	1	1	0	0	99	0
29	7	Sacrafical Alter	Where peons are sacraficed to the gods	150000	100	4	0	1	2	0	0	99	0
30	7	Sacrafical Temple	Where peons are sacraficed to the gods	1500000	200	8	0	1	4	0	0	99	0
31	8	Smith	Clang metal into useful tools and objects	500	25	0	5	1	2	0	0	99	0
32	8	Blacksmith	Blacksmith Metal tools will be the backbone of society for thousands of years to come	5000	25	0	8	1	1	0	0	99	0
33	11	Barn	A place to keep stuff	2500	600	0	575	1	10	0	0	99	0
34	11	Cold Storage	A place to keep things cool	2500	50	0	50	1	0	0	1	99	33
35	11	Dry Storage	A place to keep things dry	2500	50	0	50	1	0	0	1	99	33
36	11	Granary	A place to store grain	2500	50	0	500	1	2	0	0	99	0
37	12	Well	Everybody needs water	500	5	0	0	1	0	0	0	99	0
38	12	Aquaduct	Bringing water from the hills	250000	100	0	8	1	2	0	0	99	0
39	13	Herbalist	A herbalist garden	15000	50	0	2	1	1	0	0	99	0
40	13	Doctors Office	Sick peons need treatment to help stop the spread of disease	15000	25	0	8	1	1	0	0	99	0
41	13	Apothecary	Sick peons need treatment to help stop the spread of disease	15000	25	0	8	1	1	0	0	99	0
42	13	Mid Wifes Clinic	Increasing the success rates of births	15000	80	8	10	1	4	0	0	99	0
43	13	Dentist	Where peons have rotten teeth pulled	15000	80	8	10	1	1	0	0	99	0
44	14	Market	Where peons can buy and sell goods	2500	150	0	0	1	1	0	0	99	0
45	14	Large Market	Where peons can buy and sell goods	7500	500	0	0	1	1	4	0	99	0
46	14	Slave Market	Where peons can buy other peons for whatever purpose they want	15000	500	100	0	1	2	10	0	99	0
47	14	Bank	To store money safely is a wise choice indeed	150000	200	0	100	1	1	0	0	99	0
48	15	Alchemist	An alchemists laboratory	15000	25	0	2	1	1	0	0	99	0
49	15	Potion Master	Where potions are brewed that have a wide variety of outcomes�	50000	25	0	5	1	1	0	0	99	0
50	16	Small Brothel	All peons love to have a good time	2500	25	0	0	1	6	0	0	99	0
51	16	Large Brother	All peons love to have a good time	7500	100	0	0	1	22	0	0	99	0
52	16	Small Tavern	All peons love to have a good time	2500	25	0	5	1	4	0	0	99	0
53	16	Large Tavern	All peons love to have a good time	7500	100	0	25	1	8	0	0	99	0
54	16	Small Gardens	Flowers make the mind grow stronger	2500	25	0	0	1	1	0	0	99	0
55	16	Large Gardens	Flowers make the mind grow stronger	7500	100	0	0	1	1	0	0	99	0
56	16	Barber	Some peons may want to get their hair cut	2500	25	0	0	1	1	0	0	99	0
57	16	Small Bathhouse�	Some peons may want to be clean	5000	25	0	0	1	1	0	0	99	0
58	16	Town Hall	"Meeting place for business, signing of contracts, celebrations�etc"	25000	200	0	8	1	1	0	0	99	0
59	16	Guildhall	A place	25000	100	0	8	1	1	0	0	99	0
60	17	Small Item Jeweller	"Crafting rings, bracelets, tiara's etc from metals and stones"	18000	25	0	2	1	1	0	0	99	0
61	17	Large Item Jeweller	"Crafting crowns, ornate shields, swords, armour etc from metals and stones"	45000	25	0	8	1	1	0	0	99	0
62	17	Whitesmith	"Makes items exclusively from gold, silver and tin"	2500	25	0	2	1	1	0	0	99	0
63	18	Cooper	"Coopers make everything from washbasins to butter churns, buckets to barrels"	2500	25	0	8	1	1	0	0	99	0
64	18	Cobbler	Cobblers make shoes and boots	2500	25	0	8	1	1	0	0	99	0
65	18	Tinker	Tinkers make pots and pans	2500	25	0	8	1	1	0	0	99	0
66	18	Wheeler�	They make wheels and wagons	2500	25	0	8	1	1	0	0	99	0
67	18	Carpenter	They make everything else made of wood	2500	25	0	8	1	1	0	0	99	0
68	18	Farrier	Horse/Mount doctors and they make horse/mount shoes	2500	25	0	8	1	1	0	0	99	0
69	18	Turner	They make poles and other lathed goods	2500	25	0	8	1	1	0	0	99	0
70	18	Tailor	Making garments for the masses	2500	25	0	8	1	1	0	0	99	0
71	18	Fine Tailer	Making garments for the upper classes	8500	25	0	8	1	1	0	0	99	0
72	18	Tannery	Tanning is the process of treating skins and hides of animals to produce leather	2500	25	0	8	1	1	0	0	99	0
73	18	Charcoal Maker	Where charcoal is made	2500	25	0	8	1	1	0	0	99	0
74	18	Furrier	Crafting furs and goods using pelts	2500	25	0	8	1	1	0	0	99	0
75	18	Mason	"A person skilled in cutting, dressing, and laying stone in buildings"	2500	25	0	8	1	1	0	0	99	0
76	18	Glassmith	The crafting of items made of glass	2500	25	0	8	1	1	0	0	99	0
77	18	Leatherworker	The crafting of items made of leather	2500	25	0	8	1	1	0	0	99	0
78	18	Stables	Where wild horse/mounts are trained and bred for use by peons	15000	50000	6	10000	1	15	0	0	99	0
79	18	Clothmaker	Where basic clothes are made	2500	25	0	8	1	1	0	0	99	0
80	18	Fine Clothmaker	Fine clothes and garments are made	25000	25	0	8	1	1	0	0	99	0
81	18	Book Binder	Where books are made and bound	25000	25	0	8	1	1	0	0	99	0
82	18	Small Boatbuilder	Where fishing boats and other small boats are crafted	5000	50	0	16	1	2	0	0	99	0
83	18	Medium Boatbuilder	Where merchant boats and medium sized boats are crafted	50000	150	0	16	1	12	0	0	99	0
84	18	Large Boatbuilder	Where large ships are built	500000	500	0	16	1	24	0	0	99	0
85	18	Instrumentmaker	"Where instruments are made, giving music to your citizens"	5000	25	0	8	1	1	0	0	99	0
86	18	Painters Alcove	Art is created only by those kissed by god	15000	25	0	8	1	1	0	0	99	0
87	18	Sculptors Alcove	Creating the finest scultores	15000	25	0	8	1	1	0	0	99	0
88	18	Ropemaker	Ropes don�t grow on trees	2500	25	0	8	1	1	0	0	99	0
89	19	Mill	"Mill. Wheat, barley and rye are the staples of a peons diet, and most of them are best enjoyed milled"	2500	25	0	8	1	1	0	0	99	0
90	19	Brewery	Every peon will require alcohol	5000	50	0	8	1	1	0	0	99	0
91	19	Butcher	Where meat is harvested from butchered animals	2500	25	0	8	1	1	0	0	99	0
92	19	Smokehouse	"A place to dry, smoke or salt food for preservation"	5000	25	0	8	1	1	0	0	99	0
93	19	Bakery	Who doesn�t love cakes	2500	25	0	8	1	1	0	0	99	0
94	19	Fishmonger	Where fish are gutted and prepared to be eaten	2500	25	0	8	1	1	0	0	99	0
95	19	Spice Garden	A garden growing spices	15000	50	0	2	1	1	0	0	99	0
96	19	Piemaker	Peon Pies	5000	25	0	8	1	1	0	0	99	0
97	19	Beekeepr	Everyone loves honey	25000	50	0	8	1	2	0	0	99	0
98	20	Fletcher	"Crafting bows, quivers and arrows"	5000	25	0	8	1	1	0	0	99	0
99	20	Swordsmith	Crafting swords and other handheld weapons	5000	25	0	8	1	1	0	0	99	0
100	20	Shieldsmith	Crafting shields	5000	25	0	8	1	1	0	0	99	0
101	20	Armourer	Crafting armour	5000	25	0	8	1	1	0	0	99	0
102	20	Dojo	For hand to hand combat training	5000	25	0	0	1	1	0	0	99	0
103	20	War Beast Stable	Where beasts are trained for purposes of war	35000	50000	6	10000	1	5	5	0	99	0
104	20	Barracks	Where foot soldiers are trained	15000	100	0	0	1	0	1	0	99	0
105	20	Archery Range	Where Archers are trained	15000	100	0	0	1	0	1	0	99	0
106	20	Warship Builder	Where large ships are converted to warships	500000	500	0	16	1	24	0	0	99	0
107	20	Warship Harbour	Where warships are stored and maintained	5000000	2000	0	16	1	24	0	0	99	0
108	22	Propaganda Office	Spreading your views into that of the peons	25000	25	0	8	1	8	0	0	99	0
109	23	Map Vendor	Where maps leading to unknown bounties or nothing can be purchased by peons	25000	25	0	8	1	1	0	0	99	0
110	23	Ex Military Garb	"Peons looking for adventure can purchase armour, shields and weapons"	25000	25	0	8	1	1	0	0	99	0
111	24	Fishermans Cabin	Crafts items requires for fishing	2500	25	0	8	1	1	0	0	99	0
112	24	Fishing Docks	A dock for fishing boats	100000	1000	0	200	1	16	0	0	99	0
113	24	Oyster Rakers Cabin	A cabin used by oster rakers before going to collect oysters	2500	25	0	8	1	2	0	0	99	0
114	24	Seaweed Collector	A cabin used by a seaweed collector	2500	25	0	8	1	2	0	0	99	0
115	24	Leech Collector	A cabin used by a leech collector	2500	25	0	8	1	2	0	0	99	0
116	25	Smugglers Hideout	A place to lay low and store stolen treasures	15000	250	0	100	1	1	0	0	99	0
117	25	Rum Distillery	Where rum is made	15000	250	0	100	1	1	0	0	99	0
118	25	Pirates Bay	A harbour for pirate ships	150000	25000	16	100	1	1	0	0	99	0
119	25	Pirates Bar	A place for pirates to drink and enjoy a good whore	15000	250	0	100	1	25	0	0	99	0
120	26	School	Increasing young peons stats	15000	125	25	10	1	2	0	0	99	0
121	26	Library	Giving peons a chance to learn new things� if they can read	15000	75	0	25	1	1	0	0	99	0
122	26	Ancient Tales	Teaching young adventurers tales and folklore	15000	125	25	10	1	2	0	0	99	0
123	26	Astrology Centre	More knowledge of the stars helps navigate the seas	15000	80	0	0	1	1	0	0	99	0
124	27	Roofers Workshop	Improving the rooves of buildings	5000	25	0	8	1	1	0	0	99	0
125	27	Builders Yard	Where builders and their materials are aggregated	2500	100	0	8	1	30	0	0	99	0
126	27	Mortar Mixing Yard	Improving the quality of morter used in construction	12500	50	0	8	1	2	0	0	99	0
127	27	Bridge Engineer	An office for a bridge engineer	25000	100	0	8	1	1	0	0	99	0
128	28	District HQ	The district planning office	250000	100	0	8	1	2	0	0	99	0
129	28	Realms Guard	A form of policing for feudal lords	25000	50	0	8	1	8	0	0	99	0
130	28	Immigration Office	A way to enact immigration policy	25000	50	0	8	1	1	0	0	99	0
131	28	Tax Office	A way to change tax upon the peons	25000	50	0	8	1	1	0	0	99	0
132	29	Assasins Guild	The guild of assasins	250000	100	0	8	1	2	0	0	99	0
133	29	Thiefs Guild	The guild of thieves	250000	100	0	8	1	2	0	0	99	0
134	29	Wizards Guild	The guild of wizards	250000	100	0	8	1	2	0	0	99	0
135	29	Witches Guild	The guild of witches	250000	100	0	8	1	2	0	0	99	0
136	29	Merchants Guild	The guild of merchants	250000	100	0	8	1	2	0	0	99	0
137	29	Mages Guild	The guild of mages	250000	100	0	8	1	2	0	0	99	0
138	20	Swordsmith Smelter	Adding a smelter to the swordsmith	5000	5	0	0	1	0	0	0	99	99
139	20	Shieldsmith Smelter	Adding a smelter to the shieldsmith	5000	5	0	0	1	0	0	0	99	100
140	20	Armourer Smelter	Adding a smelter to the armourer	5000	5	0	0	1	0	0	0	99	101
141	30	Small Smelter	Turning order into ingots	5000	25	0	8	1	1	0	0	99	0
142	30	Medium Smelter	Turning order into ingots	25000	50	0	16	1	2	0	0	99	0
143	30	Large Smelter	Turning order into ingots	500000	100	0	32	1	8	0	0	99	0
144	19	Windmill	"Wiindmill. Wheat, barley and rye are the staples of a peons diet, and most of them are best enjoyed milled"	25000	50	0	8	1	2	0	0	99	0
145	18	Hourglass Maker	Craftsmen creating the hourglass	25000	50	0	8	1	2	0	0	99	0
146	19	Watermill	"Watermill. Wheat, barley and rye are the staples of a peons diet, and most of them are best enjoyed milled"	25000	50	0	8	1	2	0	0	99	0
147	18	Waterhammer	Increasing production of specific item	2500000	50	0	8	1	2	0	0	99	0
148	18	Compass Maker	Creating compases to aid in navigation at sea	2500000	25	0	8	1	2	0	0	99	0
149	18	Spectacle Maker	Creating spectacles for those short of seight	2500000	25	0	8	1	2	0	0	99	0
