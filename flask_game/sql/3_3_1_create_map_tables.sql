go
use TERRARUM

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_maps_users')
begin
drop table dbo.game_maps_users
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_map_data')
begin
drop table dbo.game_map_data
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_map_tile_owner')
begin
drop table dbo.game_map_tile_owner
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'current_game')
begin
drop table dbo.current_game
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'current_game_players')
begin
drop table dbo.current_game_players
end

go
begin
create table game_maps_users (
id int identity(1,1) primary key,
userid int not null,
mapid int not null,
raceid int not null,
start_x int not null,
start_y int not null,
is_active bit not null default 1,
is_rdy_nxt_turn bit not null default 0,
datecreated datetime not null default(getdate()),
constraint FK_GMU_UID FOREIGN KEY (userid) REFERENCES users_tbl(id),
constraint FK_GMU_MID FOREIGN KEY (mapid) REFERENCES game_maps(id),
constraint FK_GMU_RID FOREIGN KEY (raceid) REFERENCES races(id),
)
end

go
begin
create table game_map_data (
id int identity(1,1) primary key,
mapid int not null,
x int not null,
y int not null,
z float not null,
elevation int not null,
equator_distance int not null,
biomeid_a int not null,
biomeid_b int,
constraint FK_GMD_MID FOREIGN KEY (mapid) REFERENCES game_maps(id),
constraint FK_GMD_BIDA FOREIGN KEY (biomeid_a) REFERENCES biomes(id),
constraint FK_GMD_BIDB FOREIGN KEY (biomeid_b) REFERENCES biomes(id),
)
end

--go
--begin
--create index game_map_xy_index
--on game_map_data (x, y)
--end

go
begin
create table game_map_tile_owner (
id int identity(1,1) primary key,
tileid int not null,
userid int not null,
turn_created int default 0,
constraint FK_MTO_MDID FOREIGN KEY (tileid) REFERENCES game_map_data(id),
constraint FK_MTO_UID FOREIGN KEY (userid) REFERENCES users_tbl(id),
)
end

go
begin
create table current_game (
id int identity(1,1) primary key,
gameid int not null,
turn_started int default 0,
turn_suspended int,
is_live_game bit,
entrydate datetime not null default(getdate()),
constraint FK_CG_GID FOREIGN KEY (gameid) REFERENCES game_maps(id),
)
end

GO
begin
create table current_game_players (
id int identity(1,1) primary key,
userid int not null,
is_playing bit not null default 1,
turn_ready bit not null default 0,
constraint FK_CGP_UID FOREIGN KEY (userid) REFERENCES users_tbl(id),
)
END