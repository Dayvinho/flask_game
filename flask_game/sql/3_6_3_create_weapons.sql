go
use TERRARUM


go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'weapons')
begin
drop table dbo.weapons
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'legendary_weapons')
begin
drop table dbo.legendary_weapons
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'weapon_types')
begin
drop table dbo.weapon_types
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'legendary_weapon_types')
begin
drop table dbo.legendary_weapon_types
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'legendary_weapon_effects')
begin
drop table dbo.legendary_weapon_effects
end

GO
BEGIN
create table weapon_types(
id int primary key,
weapon_type nvarchar(50) unique not null,
weapon_type_desc nvarchar(max) not null,
)
ENd

GO
BEGIN
create table legendary_weapon_types(
id int primary key,
weapon_type nvarchar(50) unique not null,
)
ENd

GO
BEGIN
create table weapons(
id int identity(1,1) primary key,
weapon_typdid int not null,
weapon_name nvarchar(50) unique not null,
weapon_desc nvarchar(max) not null,
add_att int not null default 0,
add_def int not null default 0,
add_hp int not null default 0,
min_str int not null default 0,
min_dex int not null default 0,
constraint FK_W_WTID FOREIGN KEY (weapon_typdid) REFERENCES weapon_types(id),
)
end

go
begin
create table legendary_weapons(
id int identity(1,1) primary key,
lweapon_typdid int not null,
weapon_name nvarchar(100) unique not null,
weapon_desc nvarchar(max) not null,
add_hp int not null default 0,
add_att int not null default 0,
add_def int not null default 0,
add_dex int not null default 0,
add_int int not null default 0,
min_str int not null default 0,
min_dex int not null default 0,
min_int int not null default 0,
constraint FK_LW_WTID FOREIGN KEY (lweapon_typdid) REFERENCES legendary_weapon_types(id),
)
end

go

begin
create table legendary_weapon_effects(
id int identity(1,1) primary key,
l_weaponid int not null,
effect nvarchar(max) not null, --not sure how to make this work right now
constraint FK_WE_WID FOREIGN KEY (l_weaponid) REFERENCES legendary_weapons(id),
)
end