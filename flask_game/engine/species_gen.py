import numpy as np
import pandas as pd
from flask_game.engine import conf, db_engine

def generate_species(tileid, biomeid):
    df_species_biomes = pd.read_sql(f'''
            select * from species_biomes where biomeid = {biomeid}
            ''', db_engine)
    species_list = df_species_biomes.speciesid.tolist()
    species_string = '('
    for species in species_list:
        species_string = f'{species_string}{species},'
    species_string = species_string[:-1]
    species_string = f'{species_string})'
    df_species = pd.read_sql(f'''
            select * from species where id in {species_string}
            ''', db_engine)
    db_upload = []
    for _, row in df_species.iterrows():
        spawn_chance = row['spawn_chance']
        min_spawn = row['min_spawn_amount']
        max_spawn = row['max_spawn_amount']
        amount = int(np.random.uniform(min_spawn, max_spawn))
        speciesid = int(row['id'])
        if spawn_chance == 1:
            species_add = {
                'tileid': tileid,
                'speciesid': speciesid,
                'amount': amount
            }
            db_upload.append(species_add)
        else:
            species_test = np.random.uniform(low=0.0, high=1.0)
            if species_test < spawn_chance:
                species_add = {
                    'tileid': tileid,
                    'speciesid': speciesid,
                    'amount': amount
                }
                db_upload.append(species_add)
    df_insert = pd.DataFrame(db_upload)
    df_insert.to_sql('game_map_tile_species', db_engine, 
                    if_exists='append', index=False)