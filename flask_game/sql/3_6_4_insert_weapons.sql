go
use TERRARUM

go
begin
insert into weapon_types(id, weapon_type, weapon_type_desc)
values
(1, 'Arrows', 'Arrows'),
(2, 'Sheilds', 'Shields'),
(3, 'Curved One-Handed Swords', 'Swords'),
(4, 'Straight One-Handed Swords', 'Swords'),
(5, 'Curved Two-Handed swords', 'Swords'),
(6, 'Hand-And-A-Half And Two-Handed Greatswords', 'Swords'),
(7, 'Straight Shortswords', 'Swords'),
(8, 'Curved Shortswords', 'Swords'),
(9, 'Sickles', 'Farming Swords'),
(10, 'Picks and Pickaxes', 'Axes'),
(11, 'Axes', 'Axes'),
(12, 'Clubs and Blunt Weapons', 'Clubs'),
(13, 'Blunt Staves', 'Staves'),
(14, 'Spears', 'Long Weapons'),
(15, 'Polearms', 'Long Weapons'),
(16, 'Spears and Javelins', 'Long Weapons'),
(17, 'Throwing Blades and Darts', 'Thrown Weapons'),
(18, 'Throwing Axes', 'Thrown Weapons'),
(19, 'Longbows', 'Bows'),
(20, 'Recurve Bows', 'Bows'),
(21, 'Short Bows', 'Bows'),
(22, 'Crossbows', 'Bows'),
(23, 'Sling', 'Slings'),
(24, 'Axe-like Swords', 'Swords')

end

go 
BEGIN
insert into legendary_weapon_types(id, weapon_type)
VALUES
(1, 'Swords'),
(2, 'Axes'),
(3, 'Bows'),
(4, 'Thrown Weapons'),
(5, 'Long Weapons'),
(6, 'Shields'),
(7, 'Slings')
end


GO
begin
bulk insert weapons
from 'C:\FlaskGame\flask_game\flask_game\sql\weapons_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end

GO
begin
bulk insert legendary_weapons
from 'C:\FlaskGame\flask_game\flask_game\sql\legendary_weapons_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end