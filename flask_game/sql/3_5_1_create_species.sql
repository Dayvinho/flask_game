go
use TERRARUM

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'species_types')
begin
drop table dbo.species_types
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'species')
begin
drop table dbo.species
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'species_biomes')
begin
drop table dbo.species_biomes
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_species_diseases')
begin
drop table dbo.game_species_diseases
end

go 
BEGIN
create table species_types (
id int identity(1,1) primary key,
species_type nvarchar(20) unique not null,
)
END

GO
BEGIN
create table species (
id int primary key,
speciestype int,
species nvarchar(100) not null,
common_name nvarchar(100) not null,
species_desc nvarchar(max),
hp int not null default 1,
attack int not null default 1,
defence int not null default 1,
intelligence int not null default 1,
speed int not null default 1,
min_mass float not null default 10,
max_mass float not null default 20,
size float not null default 0.25,
meat float not null default 0.5, --base % of meat harvestable
max_litter_size int not null default 2,
cuteness float not null default 0.5, -- 0.5 = neutral, 1 = super cute, 0 = super scary
spawn_chance float not null default 0.3,  -- 0 -> 1.0 scale
min_spawn_amount float not null default 100,
max_spawn_amount float not null default 500,
constraint FK_B_BYID FOREIGN KEY (speciestype) REFERENCES species_types(id),
)
END

GO
BEGIN
create table species_biomes (
id int identity(1,1) primary key,
speciesid int,
biomeid int,
constraint FK_SB_BEID FOREIGN KEY (speciesid) REFERENCES species(id),
constraint FK_SB_BID FOREIGN KEY (biomeid) REFERENCES biomes(id),
)
end

GO
begin
create table species_sex_cycles (
id int identity(1,1) primary key,
speciesid int not null,
monthid int not null,
chance float not null, --% chance of occurance

constraint FK_BSC_GBID FOREIGN KEY (speciesid) REFERENCES species(id),
constraint FK_BSC_MID FOREIGN KEY (monthid) REFERENCES months(monthid),
)
END

GO
BEGIN
create table game_map_tile_species (
id int identity(1,1) primary key,
tileid int not null,
speciesid int not null,
amount int not null,
constraint FK_GB_GMTID FOREIGN KEY (tileid) REFERENCES game_map_data(id),
constraint FK_GB_BID FOREIGN KEY (speciesid) REFERENCES species(id),
)
end

go
begin
create table game_species_diseases(
id int identity(1,1) primary key,
gspeciesid int not null,
diseaseid int not null,
severity float not null default 0.1,
carrier bit not null, --yes = carrier, no = infected
constraint FK_GBD_DID FOREIGN KEY (diseaseid) REFERENCES diseases(id),
constraint FK_GBD_GBID FOREIGN KEY (gspeciesid) REFERENCES game_map_tile_species(id),
)
end

go
BEGIN
create table genus(
id int identity(1,1) primary key,
genusid int not null,
genus nvarchar(50) not null,
)
end

go
BEGIN
create table species_genus(
id int identity(1,1) primary key,
speciesid int not null,
genusid int not null,
constraint FK_SG_SID FOREIGN KEY (speciesid) REFERENCES species(id),
constraint FK_SG_GID FOREIGN KEY (genusid) REFERENCES genus(id),
)
end

