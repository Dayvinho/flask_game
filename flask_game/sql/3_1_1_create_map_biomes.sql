go
use TERRARUM

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'biomes')
begin
drop table dbo.biomes
end

go
begin
create table biomes (
id int primary key,
biome_long_name nvarchar(100) not null,
biome_name nvarchar(50) not null,
rgb1 int,
rgb2 int,
rgb3 int,
)
end