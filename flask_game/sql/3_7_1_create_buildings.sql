go
use TERRARUM


go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_buildings_farms')
begin
drop table dbo.game_buildings_farms
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_map_tile_buildings')
begin
drop table dbo.game_map_tile_buildings
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'building_biomes')
begin
drop table dbo.building_biomes
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'building_res')
begin
drop table dbo.building_res
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'buildings')
begin
drop table dbo.buildings
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'building_types')
begin
drop table dbo.building_types
end


go
begin
create table building_types (
id int identity(1,1) primary key,
b_type nvarchar(50) not null unique,
b_type_desc nvarchar(max),
)
end

go
begin
create table buildings (
id int identity(1,1) primary key,
btypeid int not null,
b_name nvarchar(50) not null unique,
b_desc nvarchar(max),
b_points_required int not null, --number of building points required to build this building.
size float not null, -- in SQM
capacity int not null default 0, --number of families that can live here
storage int not null default 0, -- in SQM
min_turns_to_build int not null default 1,
max_workers_assigned int not null default 20,
max_military_assigned int not null default 0,
max_number_of_building int not null default 0, -- 0 = infinite
biomdid int not null, -- pass 99 for all biomes
parent_building int, --pass id of parent building (for building upgrades)

constraint FK_B_BID foreign key (biomdid) references biomes(id),
constraint FK_B_BTID foreign key (btypeid) references building_types(id),
)
end


go
--resources required to build, to function or produced by building
begin
create table building_res (
id int identity(1,1) primary key,
b_id int not null,
res_actionid int not null,
foreignid int not null,
table_name nvarchar(50) not null,
amount int not null,
constraint FK_BRR_BID FOREIGN KEY (b_id) REFERENCES buildings(id),
constraint FK_BRR_RAID foreign key (res_actionid) references res_actions(id),
)
end


GO
BEGIN
create table game_map_tile_buildings (
id int identity(1,1) primary key,
bid int not null, 
tileid int not null,
constraint FK_GMTB_GMTID FOREIGN KEY (tileid) REFERENCES game_map_data(id),
constraint FK_GMTB_BID FOREIGN KEY (bid) REFERENCES buildings(id),
)
END


go
begin
create table game_buildings_farms (
id int identity(1,1) primary key,
gbid int not null,
foreignid int not null,
table_name nvarchar(50) not null,
constraint FK_GBF_GMTBID FOREIGN KEY (gbid) REFERENCES game_map_tile_buildings(id),
)
end

