go
if exists(select * from sys.foreign_keys where name = 'FK_FPP_UID')
begin
alter table site_fp_posts
drop constraint FK_FPP_UID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GMD_BIDA')
begin
alter table game_map_data
drop constraint FK_GMD_BIDA
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GMD_BIDB')
begin
alter table game_map_data
drop constraint FK_GMD_BIDB
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GMU_UID')
begin
alter table game_maps_users
drop constraint FK_GMU_UID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GMU_MID')
begin
alter table game_maps_users
drop constraint FK_GMU_MID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GMD_MID')
begin
alter table game_map_data
drop constraint FK_GMD_MID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_MTO_MDID')
begin
alter table game_map_tile_owner
drop constraint FK_MTO_MDID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_MTO_UID')
begin
alter table game_map_tile_owner
drop constraint FK_MTO_UID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_RB_RID')
begin
alter table game_res_biomes
drop constraint FK_RB_RID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_RB_BID')
begin
alter table game_res_biomes
drop constraint FK_RB_BID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_R_RTID')
begin
alter table game_resources
drop constraint FK_R_RTID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BB_BTID')
begin
alter table game_building_biomes
drop constraint FK_BB_BTID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BB_BID')
begin
alter table game_building_biomes
drop constraint FK_BB_BID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_B_BTID')
begin
alter table game_buildings
drop constraint FK_B_BTID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BRR_BID')
begin
alter table game_building_res
drop constraint FK_BRR_BID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BRR_RID')
begin
alter table game_building_res
drop constraint FK_BRR_RID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BBE_RID')
begin
alter table game_building_bit_effects
drop constraint FK_BBE_RID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_CG_GID')
begin
alter table current_game
drop constraint FK_CG_GID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GR_BRID')
begin
alter table game_races
drop constraint FK_GR_BRID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GRB_BID')
begin
alter table game_races
drop constraint FK_GRB_BID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPF_UID')
begin
alter table game_peon_families
drop constraint FK_GPF_UID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPR_UID')
begin
alter table game_peon_interactions
drop constraint FK_GPR_UID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPR_FID')
begin
alter table game_peon_interactions
drop constraint FK_GPR_FID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPR_RID')
begin
alter table game_peon_interactions
drop constraint FK_GPR_RID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPP_UID')
begin
alter table game_player_peons
drop constraint FK_GPP_UID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPP_FID')
begin
alter table game_player_peons
drop constraint FK_GPP_FID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPP_SOID')
begin
alter table game_player_peons
drop constraint FK_GPP_SOID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPF_GID')
begin
alter table game_peon_families
drop constraint FK_GPF_GID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPP_GID')
begin
alter table game_peon_families
drop constraint FK_GPP_GID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GMU_RID')
begin
alter table game_peon_families
drop constraint FK_GMU_RID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_MTB_MDID')
begin
alter table game_map_tile_r
drop constraint FK_MTB_MDID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_MTR_MDID')
begin
alter table game_map_tile_b
drop constraint FK_MTR_MDID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BB_BID')
begin
alter table beast_biomes
drop constraint FK_BB_BID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BB_BEID')
begin
alter table beast_biomes
drop constraint FK_BB_BEID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPA_PID')
begin
alter table game_peon_addictions
drop constraint FK_GPA_PID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPA_AID')
begin
alter table game_peon_addictions
drop constraint FK_GPA_AID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_GPD_PID')
begin
alter table peon_diseases
drop constraint FK_GPD_PID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BSC_MID')
begin
alter table beast_sex_cycles
drop constraint FK_BSC_MID
end

go
if exists(select * from sys.foreign_keys where name = 'FK_BSC_MID')
begin
alter table beast_sex_cycles
drop constraint FK_BSC_MID
end





;