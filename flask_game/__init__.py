from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from flask_game.config import Config

db = SQLAlchemy()
bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message_category = 'info'
mail = Mail()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)

    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)
    
    from flask_game.users.routes import users
    from flask_game.main.routes import main
    from flask_game.errors.handlers import errors
    from flask_game.game.map_routes import game_map
    from flask_game.game.create_game_routes import create_game
    from flask_game.game.game_routes import game_routes

    app.register_blueprint(users)
    app.register_blueprint(main)
    app.register_blueprint(errors)
    app.register_blueprint(game_map)
    app.register_blueprint(create_game)
    app.register_blueprint(game_routes)

    return app