GO
use terrarum


go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_map_tile_r')
begin
drop table dbo.game_map_tile_r
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_map_tile_bu')
begin
drop table dbo.game_map_tile_bu
end



go 
BEGIN
create table game_map_tile_r ( --resources
id int identity(1,1) primary key,
tileid int not null,
resid int not null,
res_amount float not null,
constraint FK_MTR_MDID FOREIGN KEY (tileid) REFERENCES game_map_data(id),
)
END

go 
BEGIN
create table game_map_tile_bu ( --buildings
id int identity(1,1) primary key,
tileid int not null,
buid int not null,
b_level int default 0,
constraint FK_GMTB_MDID FOREIGN KEY (tileid) REFERENCES game_map_data(id),
constraint FK_GMTB_BID FOREIGN KEY (buid) REFERENCES buildings(id),
)
end




;