go
use TERRARUM

go
begin
insert into races_types(race_type, race_desc)
values
('Human', 'Filthy Humans'),
('Dwarf', 'Filthy Dwarfs');
end




go
begin
declare @Human int = (select id from races_types where race_type = 'Human')
declare @Dwarf int = (select id from races_types where race_type = 'Dwarf')
--set @Human 
insert into races(race_name, race_desc, min_height, max_height, 
min_hp, max_hp, natural_avg_death_age, puberty_start, puberty_end, 
gen_family_size, baseraceid)
values
('Men', 'Men of the forest', '150', '200', '5', '20', '80', '13', '20', '8', @Human),
('Nomads', 'Men of the deserts/jungles', '150', '200', '5', '20', '80', '13', '20', '10', @Human),
('Mountain Dwarfs', 'Dwarfs of the mountains', '91', '137', '8', '22', '70', '15', '20', '6', @Dwarf);
end

--go
--BEGIN
--declare @TEF int = (select id from biomes where biome_name = 'Temperate Coniferous Forests')
--declare @TW int = (select id from biomes where biome_name = 'Temperate Broadleaf and Mixed Forests')
--declare @TS int = (select id from biomes where biome_name = 'Temperate Shrubland')
--declare @M int = (select id from biomes where biome_name = 'Montane')
--declare @EW int = (select id from biomes where biome_name = 'Elfin Woodland')
--declare @TG int = (select id from biomes where biome_name = 'Temperate Grassland')
--declare @TF int = (select id from biomes where biome_name = 'Temperate Forest')
--declare @SADDS int = (select id from biomes where biome_name = 'Semi Arid Desert - Desert Scrub')
--declare @DS int = (select id from biomes where biome_name = 'Dry Steppe')
--declare @TR int = (select id from biomes where biome_name = 'Tropical Rainforest')
--declare @TSR int = (select id from biomes where biome_name = 'Tropical Seasonal Rainforest')
--declare @D int = (select id from biomes where biome_name = 'Desert')
--declare @WTD int = (select id from biomes where biome_name = 'Warm Temperate Desert')
--declare @S int = (select id from biomes where biome_name = 'Savanna')
--declare @TD int = (select id from biomes where biome_name = 'Tropical Desert')
--declare @MTN int = (select id from biomes where biome_name = 'Mountain')
--declare @I int = (select id from biomes where biome_name = 'Ice')
--declare @T int = (select id from biomes where biome_name = 'Taiga')
--declare @TDR int = (select id from biomes where biome_name = 'Tundra')
--declare @A int = (select id from biomes where biome_name = 'Alpine')
--declare @AG int = (select id from biomes where biome_name = 'Alpine Grasslands')
--declare @HAT int = (select id from biomes where biome_name = 'High-Alt Tundra')
--declare @AAD int = (select id from biomes where biome_name = 'Arctic-Alpine Desert')
--
--declare @MEN int = (select id from races where race_name = 'Men')
--declare @NOMADS int = (select id from races where race_name = 'Nomads')
--declare @MDWARFS int = (select id from races where race_name = 'Mountain Dwarfs')
--insert into race_biomes(raceid, biomeid)
--VALUES
--(@MEN, @TEF),
--(@MEN, @TW),
--(@MEN, @TS),
--(@MEN, @M),
--(@MEN, @EW),
--(@MEN, @TG),
--(@MEN, @TF),
--
--
--(@NOMADS, @SADDS),
--(@NOMADS, @DS),
--(@NOMADS, @TR),
--(@NOMADS, @TSR),
--(@NOMADS, @D),
--(@NOMADS, @WTD),
--(@NOMADS, @S),
--(@NOMADS, @TD),
--
--
--(@MDWARFS, @MTN),
--(@MDWARFS, @I),
--(@MDWARFS, @T),
--(@MDWARFS, @TDR),
--(@MDWARFS, @A),
--(@MDWARFS, @AG),
--(@MDWARFS, @HAT),
--(@MDWARFS, @AAD)
--
--
--;
--end