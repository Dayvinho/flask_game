go
use TERRARUM


go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_peon_families')
begin
drop table dbo.game_peon_families
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_player_peons')
begin
drop table dbo.game_player_peons
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_peon_interactions')
begin
drop table dbo.game_peon_interactions
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_peon_addictions')
begin
drop table dbo.game_peon_addictions
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_peon_stat_changes')
begin
drop table dbo.game_peon_stat_changes
end


go
begin
create table game_peon_families (
id int identity(1,1) primary key,
userid int not null,
gameid int not null,
family_name nvarchar(50) not null,
start_size int not null,
turn_created int default 0,
constraint FK_GPF_UID FOREIGN KEY (userid) REFERENCES users_tbl(id),
constraint FK_GPF_GID FOREIGN KEY (gameid) REFERENCES game_maps(id),
)
end

go
begin
create table game_player_peons (
id int identity(1,1) primary key,
userid int not null,
familyid int not null,

first_name nvarchar(50) not null,
genderid int not null,

age int not null,
hp int not null, 
intelligence int not null,
strength int not null,
dexterity int not null,
wisdom int not null, 
constitution int not null,
charisma int not null,

height float not null,
sexual_orientation int not null,
attractiveness int not null,

happyness float not null default 0.5, -- 0 to 1 scale 
hunger float not null default 0.5,
thirst float not null default 0.5,
energy float not null default 0.5,
lust float not null default 0.5,

gen_status nvarchar(50),
death_turn int,
born_turn int default 0,

constraint FK_GPP_UID FOREIGN KEY (userid) REFERENCES users_tbl(id),
constraint FK_GPP_GID FOREIGN KEY (genderid) REFERENCES genders(id),
constraint FK_GPP_FID FOREIGN KEY (familyid) REFERENCES game_peon_families(id),
constraint FK_GPP_SOID FOREIGN KEY (sexual_orientation) REFERENCES sexual_orientaton(id),
)
end

go 
BEGIN
create table game_peon_interactions (
id int identity(1,1) primary key,
peon_aid int not null,
peon_bid int not null,
interactionid int not null,
int_value float default round(rand(),2), -- 0 - 1 scale, hate to love

constraint FK_GPR_UID FOREIGN KEY (peon_aid) REFERENCES game_player_peons(id),
constraint FK_GPR_FID FOREIGN KEY (peon_bid) REFERENCES game_player_peons(id),
constraint FK_GPR_RID FOREIGN KEY (interactionid) REFERENCES interactions(id),
)
end

go
BEGIN
create table game_peon_addictions (
id int identity(1,1) primary key,
peonid int not null,
addictionid int not null,
severity float not null default 0.1,

constraint FK_GPA_PID FOREIGN KEY (peonid) REFERENCES game_player_peons(id),
constraint FK_GPA_AID FOREIGN KEY (addictionid) REFERENCES addictions(id),
)
END

GO
BEGIN
create table game_peon_diseases (
id int identity(1,1) primary key,
peonid int not null,
diseaseid int not null,
severity float not null default 0.1,

constraint FK_GPD_PID FOREIGN KEY (peonid) REFERENCES game_player_peons(id),
constraint FK_GPD_DID FOREIGN KEY (diseaseid) REFERENCES diseases(id),
)
end

GO
BEGIN
create table game_peon_stat_changes (
id int identity(1,1) primary key,
peonid int not null,
stat_name nvarchar(30) not null,
stat_change float not null,
reason nvarchar(max) not null,
constraint FK_GPSC_PID FOREIGN KEY (peonid) REFERENCES game_player_peons(id),
)
end



GO
BEGIN
create table game_peon_events (
id int identity(1,1) primary key,
peonid int not null,
eventid int not null default 0,
event_desc nvarchar(max) not null,
constraint FK_GPE_PID FOREIGN KEY (peonid) REFERENCES game_player_peons(id),
constraint FK_GPE_EID FOREIGN KEY (eventid) REFERENCES events(id),
)
end
;