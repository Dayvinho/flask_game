go
begin
use master
if exists(select * from sys.databases where name='TERRARUM')
drop database TERRARUM
end

go
begin
CREATE DATABASE TERRARUM
end;