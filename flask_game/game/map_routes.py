import numpy as np
import pandas as pd
import pickle
import sys
import os
import urllib.parse
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
from flask import render_template, request, Blueprint, Markup, url_for, redirect, flash
from flask_login import login_required, current_user
from flask_game import db
from flask_game.game.forms import ConfirmNewGame, CancelNewGame, JoinGame
from flask_game.engine.map_gen import store_map
from flask_game.models import users_tbl
from plotly.offline import download_plotlyjs, plot
from plotly import offline

from flask_game.engine import conf, db_engine

game_map = Blueprint('game_map', __name__)

@game_map.route("/game_map/<game_map_id>")
@login_required
def game_map_display(game_map_id):
    #RE WORK WITH FASTER LOADING
    #ADD TOGGLES TO CONTROL OVERLAYS 
    z_data = pd.read_sql(f'''
            select x,y,z from game_map_data where mapid = {game_map_id}
            ''', db_engine)
    acty = pd.read_sql(f'''
            select act_y from game_maps where id = {game_map_id}
            ''', db_engine)
    act_y = acty['act_y'].tolist()
    act_y = act_y[0]
    z_list = z_data['z'].tolist()
    x_list = z_data['x'].tolist()
    y_list = z_data['y'].tolist()

    data = [go.Contour(z=z_list, x=x_list, y=y_list)]
    layout = go.Layout(autosize=False, width=1250, height=1250, 
                        margin=go.layout.Margin(l=50, r=50, b=100, t=100, pad=4),
                        paper_bgcolor='#303030', plot_bgcolor='#303030',
                        font=dict(color='#ffffff'),
                        title='Game Map')

    fig = go.Figure(data=data, layout=layout)

    div = offline.plot(fig, show_link=False, output_type="div")
    div = Markup(div)

    return render_template('display_map.html', 
                            title='Game Map', 
                            div=div)

@game_map.route("/game_map_peview/<game_name>", methods=['POST', 'GET'])
@login_required
def game_map_preview(game_name):
    form1 = ConfirmNewGame()
    form2 = CancelNewGame()
    if request.method == 'GET':
        df_xyz = pickle.load(open(f'{os.path.join(conf.PICKLE_PATH, game_name)}.p', 'rb'))
        z_list = df_xyz['z'].tolist()
        x_list = df_xyz['x'].tolist()
        y_list = df_xyz['y'].tolist()

        data = [go.Contour(z=z_list, x=x_list, y=y_list)]
        layout = go.Layout(autosize=False, width=1250, height=1250,
                            margin=go.layout.Margin(l=50, r=50, b=100, t=100, pad=4),
                            paper_bgcolor='#303030',
                            plot_bgcolor='#303030',
                            font=dict(color='#ffffff'),
                            title='Game Map')

        fig = go.Figure(data=data, layout=layout)

        div = offline.plot(fig, show_link=False, output_type="div")
        div = Markup(div)

    if form1.submit_confirm.data == True:
        store_map(game_name)
        return redirect(url_for('create_game.pick_start_location', 
                                game_name=game_name, 
                                userid=current_user.id))
        
    if form2.submit_cancel.data == True:
        return redirect(url_for('create_game.create_new_game'))

    return render_template('display_map_preview.html', 
                        title='Game Map', 
                        form1=form1,
                        form2=form2, 
                        div=div)
        
@game_map.route("/join_game", methods=['POST', 'GET'])
@login_required
def join_game():
    form  = JoinGame()
    game_data = pd.read_sql('select * from game_maps', db_engine)
    if request.method == 'POST':
        if form.validate_on_submit():
            gameid = form.game.data
            user_check = pd.read_sql(f'''
                    select id from game_maps_users where mapid = {gameid} and userid = {current_user.id}
                    ''', db_engine)
            if user_check.empty == True:
                game_name = pd.read_sql(f"select game_name from game_maps where id = '{gameid}'", db_engine)
                user_update = users_tbl.query.filter_by(id=current_user.id).first()
                user_update.gameid = gameid
                db.session.commit()
                game_name = game_name.game_name.item()
                return redirect(url_for('create_game.pick_start_location', 
                                    game_name=game_name, 
                                    userid=current_user.id))
            else:
                flash('User has already joined this game', 'warning')
                return redirect(url_for('game_routes.overview', 
                                    gameid=gameid, 
                                    userid=current_user.id))


    return render_template('join_game.html', form=form, tables=[game_data.to_html(classes='data')],
                            titles=game_data.columns.values)

@game_map.route("/set_current_game", methods=['POST', 'GET'])
@login_required
def set_current_game():
    pass
    return render_template('set_current_game.html', form=form)