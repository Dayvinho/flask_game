go
use TERRARUM

go
begin
insert into species_types(species_type) --this needs a rethink, no longer required in current state
VALUES
('Metal'),
('Rock'),
('Mystic'),
('Legendary'),
('Mythical')
;
end

GO
begin
bulk insert species
from 'C:\FlaskGame\flask_game\flask_game\sql\species_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end

GO
begin
bulk insert species_biomes
from 'C:\FlaskGame\flask_game\flask_game\sql\species_biomes_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end

GO
begin
bulk insert genus
from 'C:\FlaskGame\flask_game\flask_game\sql\genus_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end

GO
begin
bulk insert species_genus
from 'C:\FlaskGame\flask_game\flask_game\sql\species_genus_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end