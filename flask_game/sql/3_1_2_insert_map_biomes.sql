go
use TERRARUM

GO
begin
bulk insert biomes
from 'C:\FlaskGame\flask_game\flask_game\sql\biomes_insert.txt'
with (
FIELDTERMINATOR = '\t',
ROWTERMINATOR = '\n',
FIRSTROW=2
)
end