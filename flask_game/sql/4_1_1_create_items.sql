go
use TERRARUM


go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'species_item_drops')
begin
drop table dbo.species_item_drops
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'item_types')
begin
drop table dbo.item_types
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'items')
begin
drop table dbo.items
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'items_props')
begin
drop table dbo.items_props
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'items_craft')
begin
drop table dbo.items_craft
end

go
BEGIN
create table item_types (
id int identity(1,1) primary key,
)
END

go
BEGIN
create table items (
id int identity(1,1) primary key,
item_typeid int not null,
item_name nvarchar(40) unique not null,
item_desc nvarchar(max),
item_value float not null,

constraint FK_I_ITID FOREIGN KEY (item_typeid) REFERENCES item_types(id),
)
END

GO
BEGIN
create table items_props ( --item properties
id int identity(1,1) primary key,
itemid int not null,
property nvarchar(100) unique not null,
prop_desc nvarchar(max) unique not null,
val_to_change nvarchar(20) not null, --Core value to be changed, either DB column name or Python Variable name.
change_amount float not null, --if this need to be an int to be deducted from a core stat, set below bit value to 1
treat_as_int bit not null default 0,
constraint FK_IP_IID FOREIGN KEY (itemid) REFERENCES items(id),
)
END

GO
BEGIN
create table items_craft (
id int identity(1,1) primary key,
itemid int not null,
resid int not null,
amount float not null,
constraint FK_IC_RID FOREIGN KEY (itemid) REFERENCES resources(id),
constraint FK_IC_IID FOREIGN KEY (itemid) REFERENCES items(id),
)
END

GO
BEGIN
create table species_item_drops (
id int identity(1,1) primary key,
itemid int not null,
drop_chance float not null,
min_qty int not null,
max_qty int not null,

constraint FK_BID_IID FOREIGN KEY (itemid) REFERENCES items(id),
)
END