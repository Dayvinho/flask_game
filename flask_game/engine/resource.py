import numpy as np
import pandas as pd
from flask_game.engine import conf, db_engine

def add_start_resources(userid, gameid, resid, amount):
    if isinstance(resid, list):
        insert_list = []
        for res in resid:
            insert_data = {
                'userid': userid,
                'gameid': gameid,
                'resid': res,
                'amount': amount
            }
            insert_list.append(insert_data)
    elif isinstance(resid, int):
        insert_data = {
            'userid': userid,
            'gameid': gameid,
            'resid': resid,
            'amount': amount
        }
        insert_list.append(insert_data)
    df_insert = pd.DataFrame(insert_list, index=[0])
    df_insert.to_sql('game_player_res', db_engine, 
                        if_exists='append', index=False)