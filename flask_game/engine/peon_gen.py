import numpy as np
import pandas as pd
import flask_game.names as names
from flask_game.engine import conf, db_engine

# GENDERS MAY BECOME RACE SPECIFIC, SO A TABLE NEEDS TO 
# BE CREATED MAPPING GENDERS TO RACES THEY APPLY TO
genderids = pd.read_sql(f"select * from genders", db_engine)
gmid = genderids.id[genderids.gender == 'Male'].item()
gfid = genderids.id[genderids.gender == 'Female'].item()

df_so = pd.read_sql ('select id, orientation from sexual_orientaton', db_engine)
homoid = df_so.id[df_so.orientation == 'Homosexual'].item()
biid = df_so.id[df_so.orientation == 'Bisexual'].item()
heteroid = df_so.id[df_so.orientation == 'Heterosexual'].item()

def generate_family_sizes(start_peon_count, low, high, userid, mapid):
    total = 0
    fam_size_list = []
    gameid = mapid
    while total < start_peon_count:
        if total >= start_peon_count - high:
            high = start_peon_count - total
        if total == 999:
            total += 1
            fam_size_list.append(1)
            break
            
        fam_size = np.random.randint(low, high)
        total += fam_size
        fam_size_list.append(fam_size)

    fam_dict_list = []
    for family in fam_size_list:
        fam_dict = {
            'family_name': names.get_last_name(),
            'start_size': family,
            'userid': userid,
            'gameid': gameid,
        }
        fam_dict_list.append(fam_dict)
        
    df = pd.DataFrame(fam_dict_list)
    df.to_sql('game_peon_families', db_engine, if_exists='append', index=False)

def get_sexual_orientation(incbi=False):
    so_test = np.random.randint(1,11)
    if so_test == 10:
        sexual_orientation = homoid
    elif so_test == 9 and incbi is True:
        sexual_orientation = biid
    else:
        sexual_orientation = heteroid
    return sexual_orientation

def get_gender():
    gender_test = np.random.randint(1,3)
    if gender_test == 1:
        gender = gmid
    else:
        gender = gfid
    return gender

def get_mum_and_dad(familyid, userid):
    temp_peon_list = []
    for x in range(2):
        if x == 0:
            first_name = names.get_first_name(gender='male')
            status = 'DAD'
            gender = gmid
        else:
            first_name = names.get_first_name(gender='female')
            status = 'MUM'
            gender = gfid
        age = get_age()
        hp = get_stat()
        intelligence = get_stat()
        strength = get_stat()
        dexterity = get_stat()
        wisdom = get_stat()
        constitution = get_stat()
        charisma = get_stat()
        height = get_height()
        attractiveness = get_stat()
        peon_dict = {
            'familyid': familyid,
            'userid': userid,
            'first_name': first_name,
            'genderid': gender,
            'sexual_orientation': heteroid,
            'age': age,
            'height': height,
            'hp': hp,
            'intelligence': intelligence,
            'strength': strength,
            'dexterity': dexterity,
            'wisdom': wisdom,
            'constitution': constitution,
            'charisma': charisma,
            'attractiveness': attractiveness,
            'gen_status': status
            }
        temp_peon_list.append(peon_dict)
    return temp_peon_list

def is_siblings():
    sib_test = np.random.randint(1,6)
    if sib_test == 1:
        return True
    else:
        return False
    
def get_age(low=20, high=60):
    age = np.random.randint(low, high)
    return age

def get_stat(low=1, high=20):
    if high < 2:
        high = 2
    stat = np.random.randint(low, high)
    return stat

def get_height(age=None, low=150, high=200):
    if age is None:
        height = np.random.randint(low, high)
    elif age == 1:
        low = 30
        high = 90
    elif age == 2:
        low = 40
        high = 100
    elif age == 3:
        low = 60
        high = 110
    elif age == 4:
        low = 80
        high = 120
    elif age == 5:
        low = 90
        high = 128
    elif age == 6:
        low = 100
        high = 135
    elif age == 7:
        low = 108
        high = 142
    elif age == 8:
        low = 96
        high = 150
    elif age == 9:
        low = 104
        high = 153
    elif age == 10:
        low = 110
        high = 155
    elif age == 11:
        low = 116
        high = 160
    elif age == 12:
        low = 121
        high = 165
    elif age == 13:
        low = 126
        high = 170
    elif age == 14:
        low = 131
        high = 175
    elif age == 15:
        low = 136
        high = 180
    elif age == 16:
        low = 141
        high = 185
        
    height = np.random.randint(low, high)
    return height


def generate_family_members(userid, gameid):
    df_famids = pd.read_sql(f'''
                select id, start_size from game_peon_families 
                where userid = {userid} and gameid = {gameid}
                ''', db_engine)
    peon_dict_list = []
    for familyid, family_size in list(zip(df_famids['id'], df_famids['start_size'])):

        if family_size == 1:

            gender = get_gender()
            if gender == gmid:
                first_name = names.get_first_name(gender='male')
            else:
                first_name = names.get_first_name(gender='female')
            sexual_orientation = get_sexual_orientation(incbi=True)
            age = get_age()
            hp = get_stat()
            intelligence = get_stat()
            strength = get_stat()
            dexterity = get_stat()
            wisdom = get_stat()
            constitution = get_stat()
            charisma = get_stat()
            height = get_height()
            attractiveness = get_stat()
            peon_dict = {
                'familyid': familyid,
                'userid': userid,
                'first_name': first_name,
                'genderid': gender,
                'sexual_orientation': sexual_orientation,
                'age': age,
                'height': height,
                'hp': hp,
                'intelligence': intelligence,
                'strength': strength,
                'dexterity': dexterity,
                'wisdom': wisdom,
                'constitution': constitution,
                'charisma': charisma,
                'attractiveness': attractiveness,
                'gen_status': 'SINGLE'
            }
            peon_dict_list.append(peon_dict)
                
        elif family_size == 2:
            siblings = is_siblings()
            
            if siblings is False:
                sexual_orientation = get_sexual_orientation()
                
                if sexual_orientation == homoid:
                    gender = get_gender()
                    
                    for x in range(family_size):
                        
                        if gender == gmid:
                            first_name = names.get_first_name(gender='male')
                        else:
                            first_name = names.get_first_name(gender='female')
                        age = get_age()
                        hp = get_stat()
                        intelligence = get_stat()
                        strength = get_stat()
                        dexterity = get_stat()
                        wisdom = get_stat()
                        constitution = get_stat()
                        charisma = get_stat()
                        height = get_height()
                        attractiveness = get_stat()
                        peon_dict = {
                            'familyid': familyid,
                            'userid': userid,
                            'first_name': first_name,
                            'genderid': gender,
                            'sexual_orientation': sexual_orientation,
                            'age': age,
                            'height': height,
                            'hp': hp,
                            'intelligence': intelligence,
                            'strength': strength,
                            'dexterity': dexterity,
                            'wisdom': wisdom,
                            'constitution': constitution,
                            'charisma': charisma,
                            'attractiveness': attractiveness,
                            'gen_status': 'HOMO COUPLE'
                        }
                        peon_dict_list.append(peon_dict)
                        
                elif sexual_orientation == heteroid:
                    
                    mum_dad = get_mum_and_dad(familyid, userid)
                    
                    for peon_dict in mum_dad:
                        peon_dict_list.append(peon_dict)
            else:
                for x in range(family_size):
                    gender = get_gender()
                    sexual_orientation = get_sexual_orientation(incbi=True)
                    if gender == gmid:
                        first_name = names.get_first_name(gender='male')
                    else:
                        first_name = names.get_first_name(gender='female')
                    age = get_age()
                    hp = get_stat()
                    intelligence = get_stat()
                    strength = get_stat()
                    dexterity = get_stat()
                    wisdom = get_stat()
                    constitution = get_stat()
                    charisma = get_stat()
                    height = get_height()
                    attractiveness = get_stat()
                    peon_dict = {
                        'familyid': familyid,
                        'userid': userid,
                        'first_name': first_name,
                        'genderid': gender,
                        'sexual_orientation': sexual_orientation,
                        'age': age,
                        'height': height,
                        'hp': hp,
                        'intelligence': intelligence,
                        'strength': strength,
                        'dexterity': dexterity,
                        'wisdom': wisdom,
                        'constitution': constitution,
                        'charisma': charisma,
                        'attractiveness': attractiveness,
                        'gen_status': 'SIBLINGS'
                    }
                    peon_dict_list.append(peon_dict)
                    
        elif family_size > 2:
            
            mum_dad = get_mum_and_dad(familyid, userid)
            for peon_dict in mum_dad:
                peon_dict_list.append(peon_dict)
                
            temp_peon_list = []
            for x in range(family_size - 2):
                gender = get_gender()
                sexual_orientation = get_sexual_orientation(incbi=True)
                if gender == gmid:
                    first_name = names.get_first_name(gender='male')
                else:
                    first_name = names.get_first_name(gender='female')
                age = get_age(low=1, high=16)
                hp = get_stat(high=age)
                intelligence = get_stat(high=age)
                strength = get_stat(high=age)
                dexterity = get_stat(high=age)
                wisdom = get_stat(high=age)
                constitution = get_stat(high=age)
                charisma = get_stat(high=age)
                attractiveness = get_stat(high=age)
                height = get_height(age)
                peon_dict = {
                    'familyid': familyid,
                    'userid': userid,
                    'first_name': first_name,
                    'genderid': gender,
                    'sexual_orientation': sexual_orientation,
                    'age': age,
                    'height': height,
                    'hp': hp,
                    'intelligence': intelligence,
                    'strength': strength,
                    'dexterity': dexterity,
                    'wisdom': wisdom,
                    'constitution': constitution,
                    'charisma': charisma,
                    'attractiveness': attractiveness,
                    'gen_status': 'KIDS'
                    }
                temp_peon_list.append(peon_dict)
                
            for peon_dict in temp_peon_list:
                peon_dict_list.append(peon_dict)
                

    df2 = pd.DataFrame(peon_dict_list)
    #df2.to_csv('Test.csv')
    df2.to_sql('game_player_peons', db_engine, if_exists='append', index=False)

def generate_interactions(userid, gameid):
    df = pd.read_sql(f'''
            select p1.id as peon_aid,
            p2.id as peon_bid,
            case 
            when p1.gen_status = 'MUM' and p2.gen_status = 'DAD' 
            then (select id from interactions as i where i.interaction = 'Husband')
            when p1.gen_status = 'MUM' and p2.gen_status = 'KIDS' and p2.genderid = 1 
            then (select id from interactions as i where i.interaction = 'Son' )
            when p1.gen_status = 'MUM' and p2.gen_status = 'KIDS' and p2.genderid = 2 
            then (select id from interactions as i where i.interaction = 'Daughter')
            when p1.gen_status = 'DAD' and p2.gen_status = 'MUM' 
            then (select id from interactions as i where i.interaction = 'Wife')
            when p1.gen_status = 'DAD' and p2.gen_status = 'KIDS' and p2.genderid = 1 
            then (select id from interactions as i where i.interaction = 'Son')
            when p1.gen_status = 'DAD' and p2.gen_status = 'KIDS' and p2.genderid = 2 
            then (select id from interactions as i where i.interaction = 'Daughter')
            when p1.gen_status = 'KIDS' and p2.gen_status = 'MUM' 
            then (select id from interactions as i where i.interaction = 'Mother')
            when p1.gen_status = 'KIDS' and p2.gen_status = 'DAD' 
            then (select id from interactions as i where i.interaction = 'Father')
            when p1.gen_status = 'KIDS' and p2.gen_status = 'KIDS' and p2.genderid = 1 
            then (select id from interactions as i where i.interaction = 'Brother')
            when p1.gen_status = 'KIDS' and p2.gen_status = 'KIDS' and p2.genderid = 2 
            then (select id from interactions as i where i.interaction = 'Sister')
            when p1.gen_status = 'SIBLINGS' and p2.gen_status = 'SIBLINGS' and p2.genderid = 1 
            then (select id from interactions as i where i.interaction = 'Brother')
            when p1.gen_status = 'SIBLINGS' and p2.gen_status = 'SIBLINGS' and p2.genderid = 2 
            then (select id from interactions as i where i.interaction = 'Sister')
            when p1.gen_status = 'HOMO COUPLE' and p2.gen_status = 'HOMO COUPLE' 
            then (select id from interactions as i where i.interaction = 'Partner')
            else 'None' end as interactionid from game_player_peons as p1
            inner join game_player_peons as p2 on p1.familyid = p2.familyid
            inner join game_peon_families as f on p1.familyid = f.id
            where p1.id <> p2.id
            and f.gameid = {gameid}
            and f.userid = {userid}
            ''', db_engine)
    df.to_sql('game_peon_interactions', db_engine, if_exists='append', index=False)