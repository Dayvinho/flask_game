import pandas as pd
import numpy as np
from flask_game.engine import conf, db_engine

def generate_resources(tileid, biomeid):
    df_res_biomes = pd.read_sql(f'''
                select * from res_biomes where biomeid = {biomeid}
                ''', db_engine)
    db_upload = []
    for _, row in df_res_biomes.iterrows():
        spawn_chance = row['spawn_chance'].item()
        min_spawn = row['min_spawn_amount'].item()
        max_spawn = row['max_spawn_amount'].item()
        amount = int(np.random.uniform(min_spawn, max_spawn))
        resid = int(row['resid'].item())
        if spawn_chance == 1:
            res_add = {
                'resid': resid,
                'tileid': tileid,
                'amount': amount,
            }
            db_upload.append(res_add)
        else:
            res_test = np.random.uniform(low=0.0, high=1.0)
            if res_test < spawn_chance:
                res_add = {
                    'resid': resid,
                    'tileid': tileid,
                    'amount': amount,
                }
                db_upload.append(res_add)
    df_insert = pd.DataFrame(db_upload)
    df_insert.to_sql('game_map_tile_res', db_engine, if_exists='append', index=False)
        