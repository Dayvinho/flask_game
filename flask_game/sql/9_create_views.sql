CREATE SCHEMA [game] AUTHORIZATION [dbo]

CREATE VIEW vw_peon_family_relations
as
SELECT
gpf.family_name as 'Family Name', 
gppa.first_name as 'First Name',
gi.interaction relation, 
gppb.first_name as 'Relation First Name', 
gpi.int_value as 'Relation Value',
gpf.userid,
gpf.gameid
FROM game_peon_interactions gpi
inner join game_player_peons gppa on gpi.peon_aid = gppa.id
inner join game_player_peons gppb on gpi.peon_bid = gppb.id
inner join interactions gi on gpi.interactionid = gi.id
inner join game_peon_families gpf on gppa.familyid = gpf.id
where gi.interaction in ('Brother','Father','Husband','Mother',
                        'Partner','Sister','Wife')

CREATE VIEW vw_peon_info
as
select family_name as 'Family Name', 
first_name as 'First Name', 
g.gender as Gender, 
so.orientation as Orientation, 
age as Age, 
hp as HP, 
intelligence as 'Int', 
strength as 'Str', 
dexterity as Dex, 
wisdom as Wis, 
constitution as Con, 
charisma as Cha, 
height as Height, 
gpf.userid,
gpf.gameid
from game_player_peons as gpp
inner join game_peon_families as gpf
on gpp.familyid = gpf.id
inner join sexual_orientaton as so 
on gpp.sexual_orientation = so.id
inner join genders as g on gpp.genderid = g.id


CREATE VIEW vw_player_farms_count
as
SELECT r.res_name as 'Resource'
, count(r.res_name) as 'Count'
, gmto.userid
, gm.id as gameid
FROM [TERRARUM].[dbo].[game_buildings_farms] as gbf

inner join resources as r on r.id = gbf.foreignid
inner join game_map_tile_buildings as gmtb on gbf.gbid = gmtb.id
inner join game_map_tile_owner as gmto on gmtb.tileid = gmto.tileid
inner join game_map_data as gmd on gmto.tileid = gmd.id
inner join game_maps as gm on gmd.mapid = gm.id
where table_name = 'resources'
group by r.res_name, gmto.userid, gm.id

CREATE VIEW vw_player_buildings_count
AS
SELECT b.b_name as 'Building'
, count(b.b_name) as 'Count'
, gmto.userid
, gm.id as gameid
FROM game_map_tile_buildings as gmtb
inner join buildings as b on gmtb.bid = b.id
inner join game_map_tile_owner as gmto on gmtb.tileid = gmto.tileid
inner join game_map_data as gmd on gmto.tileid = gmd.id
inner join game_maps as gm on gmd.mapid = gm.id
group by b.b_name, gmto.userid, gm.id

CREATE VIEW vw_player_tiles_count
as
SELECT gmto.userid
, gm.id as gameid
, count(userid) as 'Count'
FROM game_map_tile_owner as gmto
inner join game_map_data as gmd on gmto.tileid = gmd.id
inner join game_maps as gm on gmd.mapid = gm.id
group by userid, gm.id

CREATE VIEW vw_player_family_count
AS
SELECT userid
, gameid
, count(userid) as 'Count'
FROM [TERRARUM].[dbo].[game_peon_families]
group by userid, gameid

create VIEW vw_player_peon_agg
as 
select
count(gpp.id) as "peon_count",
avg(gpp.happyness) as "happy",
avg(gpp.hunger) as "hunger",
avg(gpp.thirst) as "thirst",
avg(gpp.energy) as "energy",
avg(gpp.lust) as "lust",
gpp.userid,
gpf.gameid
from game_player_peons as gpp
inner join game_peon_families as gpf on gpp.familyid = gpf.id
group by gpp.userid, gpf.gameid

create view vw_player_peon_gender
as
select 
count(gpp.id) as "peon_count",
g.gender,
gpp.userid,
gpf.gameid
from game_player_peons as gpp
inner join game_peon_families as gpf on gpp.familyid = gpf.id
inner join genders as g on gpp.genderid = g.id
group by g.gender, gpp.userid, gpf.gameid

create view vw_player_peon_so
AS
select 
count(gpp.id) as "peon_count",
so.orientation,
gpp.userid,
gpf.gameid
from game_player_peons as gpp
inner join game_peon_families as gpf on gpp.familyid = gpf.id
inner join sexual_orientaton as so on gpp.sexual_orientation = so.id
group by so.orientation, gpp.userid, gpf.gameid

create VIEW vw_player_tot_cap
AS
SELECT sum(b.capacity) as tot_cap
, gmto.userid
, gmd.mapid as gameid
FROM game_map_tile_buildings as gmtb
inner join game_map_tile_owner as gmto on gmtb.tileid = gmto.tileid
inner join game_map_data as gmd on gmto.tileid = gmd.id
inner join buildings as b on gmtb.bid = b.id
where b.capacity > 0
group by gmto.userid, gmd.mapid

create view vw_player_tot_avail_res
as 
SELECT r.res_name
, rt.res_type
, sum(amount) as tot_available
, rt.res_unit_type
, gmto.userid
, gmd.mapid as gameid
FROM game_map_tile_res as gmtr 
inner join resources as r on gmtr.resid = r.id
inner join res_types as rt on r.res_typeid = rt.id
inner join game_map_tile_owner as gmto on gmtr.tileid = gmto.tileid
inner join game_map_data as gmd on gmto.tileid = gmd.id
group by r.res_name, rt.res_type, rt.res_unit_type, gmto.userid, gmd.mapid

create view vw_player_tot_avail_species
as 
SELECT s.common_name
, sum(amount) as tot_amount
, gmto.userid
, gmd.mapid as gameid
FROM [TERRARUM].[dbo].[game_map_tile_species]
as gmts 
inner join species as s on gmts.speciesid = s.id
inner join game_map_tile_owner as gmto on gmts.tileid = gmto.tileid
inner join game_map_data as gmd on gmto.tileid = gmd.id
group by s.common_name, gmto.userid, gmd.mapid

GO

                        