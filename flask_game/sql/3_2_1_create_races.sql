go
use TERRARUM

--go
--if exists(select * from sys.foreign_keys where name = 'FK_')
--begin
--alter table game_res_biomes
--drop constraint FK_
--end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'base_races')
begin
drop table dbo.base_races
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'races')
begin
drop table dbo.races
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'race_biomes')
begin
drop table dbo.race_biomes
end



go 
BEGIN
create table races_types (
id int identity(1,1) primary key,
race_type nvarchar(30) not null unique,
race_desc nvarchar(max) not null,
)
END

go
begin
create table races (
id int identity(1,1) primary key,
race_name nvarchar(30) not null unique,
race_desc nvarchar(max) not null,
min_height float not null,
max_height float not null,
min_hp int not null,
max_hp int not null,
natural_avg_death_age float not null,
puberty_start int not null,
puberty_end int not null,
gen_family_size int not null,
gen_peon_count int default 1000,
baseraceid int not null,
constraint FK_GR_BRID FOREIGN KEY (baseraceid) REFERENCES races_types(id),
)
end

go
begin
create table race_biomes (
id int identity(1,1) primary key,
raceid int not null,
biomeid int not null,
constraint FK_GRB_BID FOREIGN KEY (biomeid) REFERENCES biomes(id),
)
end