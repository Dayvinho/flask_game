go
use TERRARUM

go
begin
insert into users_tbl(username, email, image_file, passw)
values 
('Test', 'test@test.com', 'default.jpg', '$2b$12$6qj1s2XsjCweQnqp7hqGeODFLBiDiPyjwIBC8dGCWhCmspBiz4e1C'),
('Dancing',	'justinleemorales@gmail.com', 'default.jpg', '$2b$12$D9UTfdMiJlyMszsNpTO.VutyADhEdsIwEaMke7FKFvafhbpGikcZS'),
('Shroomer', 'innesmakinen16@outlook.com', 'default.jpg', '$2b$12$cUrziGjzHvShdIA72P2c8e9wwintsZYDMNaYPti7qJpF6yl7Sx9pO'),
('bLuTest',	'blank@blank.com', 'default.jpg', '$2b$12$Y8QP3WqE.FkUJMYe.7xAZeLvMCMcfirv/N76X3LEbGiPLtfuWzFIS'),
('Test2', 'test2@test.com', 'default.jpg', '$2b$12$QWlNTlTMUuqeg1h6Frg2Iu2XqCIl2DTdYdOvjzbDgWFGq8Hu075NO'),
('Test3', 'test3@test.com',	'default.jpg', '$2b$12$xRYWqO1plr/G76BZMoRiPOTjoeujOhXymSzxcNd1xrc1NywyRaD42'),
('Vespahawk', 'joshbritton1900@gmail.com', 'default.jpg', '$2b$12$vEOx4MwVf2tu5QVOp9y2e.wKFkxnhe3YbA7ASF1cmwmE7z5KcWIAy'),
('subtlerod', 'subtlerod@yahoo.com', 'default.jpg', '$2b$12$lcJ1MrxAFZOo60kug29dGOim6Q7qqWlWeRO78AtnXfiNeur39nzcu')
end

go
BEGIN
insert into site_fp_posts(userid, title, post)
values
(1,
'Game is currently in pre-Alpha',
'The game is currently in pre-alpha stage, so lets not expect it to do much...')
;
end

go
BEGIN
insert into months(monthid, month_name)
values
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');
end

go
begin
insert into sexual_orientaton(orientation)
values
('Heterosexual'),
('Homosexual'),
('Bisexual');
end

go
begin
insert into genders(gender)
values
('Male'),
('Female');
end

go
begin
insert into interactions(interaction)
values
('Father'),
('Mother'),
('Brother'),
('Sister'),
('Husband'),
('Wife'),
('Partner'),
('Friend'),
('Coworker'),
('Son'),
('Daughter'),
('Nemesis'),
('Foe')

;
end

go
BEGIN
insert into body_parts(
limb_name, death_chance, prod_impact, att_impact, str_impact, int_impact, cha_impact, req)
VALUES
('Head', 1, 0, 0, 0, 0, 0, 'None'),
('Left Hand', 0.05, 0.5, 3, 5, 0, 0, 'Left Arm'),
('Right Hand', 0.05, 0.5, 3, 5, 0, 0, 'Right Arm'),
('Left Arm', 0.25, 0.6, 4, 6, 0, 0, 'None'),
('Right Arm', 0.25, 0.6, 4, 6, 0, 0, 'None'),
('Left Foot', 0.05, 0.5, 2, 5, 0, 0, 'Left Leg'),
('Right Foot', 0.05, 0.5, 2, 5, 0, 0, 'Left Leg'),
('Left Leg', 0.25, 0.6, 2, 6, 0, 0, 'None'),
('Right Leg', 0.25, 0.6, 2, 6, 0, 0, 'None'),
('Nose', 0, 0, 10, 0, 0, 0, 'None'),
('Left Ear', 0, 0.2, 2, 0, 0, 0, 'None'),
('Right Ear', 0, 0.2, 2, 0, 0, 0, 'None'),
('Tongue', 0.2, 0.1, 5, 0, 2, 13, 'None'),
('Left Eye', 0.3, 0.4, 5, 0, 0, 3, 'None'),
('Right Eye', 0.3, 0.4, 5, 0, 0, 3, 'None')
;
end
