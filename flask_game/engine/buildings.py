import numpy as np
import pandas as pd
from flask_game.engine import conf, db_engine


def refresh_building_data():
    df_building_data = pd.read_sql('''
            select * from buildings
            ''', db_engine)
    return df_building_data

def add_start_buildings(tileid, df_building_data):
    small_house = df_building_data.id[df_building_data.b_name == 'Small House'].item()
    medium_house = df_building_data.id[df_building_data.b_name == 'Medium House'].item()
    forester = df_building_data.id[df_building_data.b_name == 'Forester'].item()
    farm = df_building_data.id[df_building_data.b_name == 'Farm'].item()
    quarry = df_building_data.id[df_building_data.b_name == 'Quarry'].item()
    well = df_building_data.id[df_building_data.b_name == 'Well'].item()
    mason = df_building_data.id[df_building_data.b_name == 'Mason'].item()
    builders_yard = df_building_data.id[df_building_data.b_name == 'Builders Yard'].item()

    buildings = [small_house, medium_house, forester, farm, quarry, well, mason, builders_yard]
    amounts = [150, 10, 1, 10, 1, 10, 1, 2]

    df_list = []
    for b, a in zip(buildings, amounts):
        for _ in range(a):
            insert_data = {
                'bid': b,
                'tileid': tileid
            }
            df_list.append(insert_data)
    df_insert = pd.DataFrame(df_list)
    df_insert.to_sql('game_map_tile_buildings', db_engine, 
                    if_exists='append', index=False)


def farm_produce_select(gbids, foreignid, table_name):
    for gbid in gbids:
        if isinstance(foreignid, list):
            fid = np.random.choice(foreignid)
        elif isinstance(foreignid, int):
            fid = foreignid
        
        insert_data = {
            'gbid': gbid,
            'foreignid': fid,
            'table_name': table_name
        }

        df_insert = pd.DataFrame(insert_data, index=[0])
        df_insert.to_sql('game_buildings_farms', db_engine, 
                        if_exists='append', index=False)

def building_resource_check(userid, bid):
    pass
    # check if player has enough resources to build or view building.

def build_building(bid, tileid, amount):
    df_list = []
    for _ in range(amount):
        insert_data = {
                'bid': bid,
                'tileid': tileid
            }
        df_list.append(insert_data)
    df_insert = pd.DataFrame(df_list)
    df_insert.to_sql('game_map_tile_buildings', db_engine, 
                    if_exists='append', index=False)
    