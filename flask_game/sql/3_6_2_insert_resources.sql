go
use TERRARUM
-- is_mineable
-- is_refineable
-- is_seasonal
-- is_harvestable
-- is_farmable
-- is_food
-- is_drinkable
-- is_worshipped
-- is_spoilable
-- is_cookable
-- is_preserevable
-- is_light_magic
-- is_dark_magic
-- is_muh_magic
-- is_home_item
-- is_church_item
-- is_weapon
-- is_armour
-- is_shield
-- is_rideable
-- is_trainable
-- is_religious_item
-- 
go
begin
insert into res_types(id, res_type, res_unit_type)
values
(1, 'Ore', 'KG'),
(2, 'Minerals', 'KG'),
(3, 'Refined Ore', 'Ingots'),
(4, 'Grain', 'KG'),
(5, 'Refined Grain', 'KG'),
(6, 'Wood', 'Trees'),
(7, 'Refined Wood', 'Trees'),
(8, 'Stone', 'KG'),
(9, 'Clay', 'KG'),
(10, 'Vegetable', 'KG'),
(11, 'Fruit', 'KG'),
(12, 'Legume', 'KG'),
(13, 'Pottery', 'Pieces'),
(14, 'Houseware', 'Pieces'),
(15, 'Diamonds', 'Diamonds'),
(16, 'Rubies', 'Rubies'),
(17, 'Leather', 'Pieces')
;

end

go
begin
declare @Ore int = (select id from res_types where res_type = 'Ore')
declare @Minerals int = (select id from res_types where res_type = 'Minerals')
declare @Grain int = (select id from res_types where res_type = 'Grain')
declare @Wood int = (select id from res_types where res_type = 'Wood')
declare @RefinedWood int = (select id from res_types where res_type = 'Refined Wood')
declare @Stone int = (select id from res_types where res_type = 'Stone')
declare @Clay int = (select id from res_types where res_type = 'Clay')
declare @Vegetable int = (select id from res_types where res_type = 'Vegetable')
declare @Fruit int = (select id from res_types where res_type = 'Fruit')
declare @Pottery int = (select id from res_types where res_type = 'Pottery')
declare @Legume int = (select id from res_types where res_type = 'Legume')
declare @Houseware int = (select id from res_types where res_type = 'Houseware')

insert into resources(res_name, res_desc, res_typeid)
values
-- Ores
('Iron', 'Iron Ore, to be refined into something useful', 1),
('Copper', 'Iron Ore, to be refined into something useful', 1),
('Gold', 'Iron Ore, to be refined into something useful', 1),
('Silver', 'Iron Ore, to be refined into something useful', 1), 
('Mythril', 'Iron Ore, to be refined into something useful', 1), 
('Platinum', 'Iron Ore, to be refined into something useful', 1), 
('Aluminium', 'Iron Ore, to be refined into something useful', 1), 
('Tin', 'Iron Ore, to be refined into something useful', 1),
('Diamond', 'Diamonds can be mined here! $$$', 1),
('Zinc', 'Zinc Ore, to be refined into some useful', 1),
('Ruby', 'Rubies can be mines here! $$', 1),

--Minerals --lets not get into an argument about whats an ore and whats a mineral, 
-- for this purpose minerals are bi-products of refining ores, a % of the ore will 
-- become the primary ingot ie iron = iron ingot and a % of the ore will become a 
-- mineral or minerals, so maybe 10kg of iron ore = 1kg iron ingot and 0.2kg of x, y and z minerals.
-- minerals also contain all possible gemstones now... :S
('Sulfur', 'A common bi-product of many desired ores, such as Iron, Copper and Zinc', 2),
('Quartz', 'A very common mineral', 2),
('Graphite', 'Used in pencils ', 2),
('Magnesium', 'A very common mineral', 2),
('Salt', 'mmm salt', 2),
('Topaz', 'Gemstone', 2),
('Amethyst', 'Gemstone', 2),
('Turqoise', 'Gemstone', 2),
('Sapphire', 'Gemstone', 2),
('Amber', 'Gemstone', 2),
('Jade', 'Gemstone', 2),
('Pearl', 'Gemstone', 2),
('Emerald', 'Gemstone', 2),
('Opal', 'Gemstone', 2),
('Moonstone', 'Gemstone', 2),
('Tourmaline', 'Gemstone', 2),
('Pyrite', 'Gemstone', 2),
('Rose Quartz', 'Gemstone', 2),
--onyx is a stone...
('Malachite', 'Gemstone', 2),
('Tanzanite', 'Gemstone', 2),
('Citrine', 'Gemstone', 2),
('Aquamarine', 'Gemstone', 2),
('Zircon', 'Gemstone', 2),
('Fluorite', 'Used in glass making etc.', 2),
--clay bi products
('Talc', 'Used in beauty products and some building tasks', 2),


--refined ores
('Refined Iron', 'Iron ingots, to be used in crafting', 3),
('Refined Copper', 'Copper ingots, to be used in crafting', 3),
('Refined Gold', 'Gold ingots, to be used in crafting', 3),
('Refined Silver', 'Silver ingots, to be used in crafting', 3),
('Refined Mythril', 'Mythril ingots, to be used in crafting', 3),
('Refined Platinum', 'Platinum ingots, to be used in crafting', 3),
('Refined Aluminium', 'Aluminium ingots, to be used in crafting', 3),
('Refined Tin', 'Tin ingots, to be used in crafting', 3),
('Refined Zinc', 'Zinc ingots, to be used in crafting', 3),
--these need own type
('Refined Diamond', 'Diamonds to be used in Jewelry or sold', 15),
('Refined Ruby', 'Rubies to be used in Jewelry or sold', 16),


-- Grains
('Corn', 'Corn grain, to be refined into something more useful', @Grain),
('Wheat', 'Wheat grain, to be refined into something more useful', @Grain),
('Oat', 'Oat grain, to be refined into something more useful', @Grain),
('Rice', 'Rice grain, to be refined into something more useful', @Grain),
('Barley', 'Barley grain, to be refined into something more useful', @Grain),


-- Woods & Refined Woods
('Wood', 'Freshly chopped wood, to be chopped into firewood or planks', @Wood),
('Timber', 'Wood refined into planks for construction etc.', @RefinedWood),
('Hardwood', 'Freshly chopped hardwood, to be chopped into planks', @Wood),
('Hardwood Timber', 'Wood refined into planks for construction etc.', @RefinedWood),
('Firewood', 'Wood for the fire', @RefinedWood),
('Bamboo', 'Wood for the fire', @Wood),


-- Stones
('Stone', 'Stone, to be used in many ways', @Stone),
('Granite', 'An exquisite stone', @Stone),
('Marble', 'An even more exquisite stone', @Stone),
('Onyx', 'An exquisite stone', @Stone),
('Slate', 'An exquisite stone', @Stone),


-- Clays
('Common Clay', 'Clay, to be used in many ways', @Clay),
('Stoneware Clay', 'Clay, to be used in many ways', @Clay),
('Kaolin Clay', 'Clay, to be used in many ways', @Clay),


-- Vegetables
('Potatoe', 'Potatoes, the staple of any peasants diet', @Vegetable),
('Spinach', 'Spinach, the staple of any peasants diet', @Vegetable),
('Cabbage', 'Cabbage, the staple of any peasants diet', @Vegetable),
('Broccoli', 'Broccoli, the staple of any peasants diet', @Vegetable),
('Pumpkin', 'Pumpkin, the staple of any peasants diet', @Vegetable),
('Sweet Potatoe', 'Sweet Potatoe, the staple of any peasants diet', @Vegetable),
('Asparagus', 'Asparagus, the staple of any peasants diet', @Vegetable),
('Onion', 'Onion, the staple of any peasants diet', @Vegetable),
('Garlic', 'Garlic, the staple of any peasants diet', @Vegetable),
('Cauliflower', 'Cauliflower, the staple of any peasants diet', @Vegetable),
('Zucchini', 'zucchini, the staple of any peasants diet', @Vegetable),
('Lettuce', 'Lettuce, the staple of any peasants diet', @Vegetable),
('Cucumber', 'Cucumber, the staple of any peasants diet', @Vegetable),


-- Fruit
('Strawberry', 'MMMM MUCH TASTE', @Fruit),
('Apple', 'MMMM MUCH TASTE', @Fruit),
('Orange', 'MMMM MUCH TASTE', @Fruit),
('Grapefruit', 'MMMM MUCH TASTE', @Fruit),
('Mandarin', 'MMMM MUCH TASTE', @Fruit),
('Lime', 'MMMM MUCH TASTE', @Fruit),
('Lemon', 'MMMM MUCH TASTE', @Fruit),
('Nectarine', 'MMMM MUCH TASTE', @Fruit),
('Apricot', 'MMMM MUCH TASTE', @Fruit),
('Peaches', 'MMMM MUCH TASTE', @Fruit),
('Banana', 'MMMM MUCH TASTE', @Fruit),
('Mango', 'MMMM MUCH TASTE', @Fruit),
('Pear', 'MMMM MUCH TASTE', @Fruit),
('Plum', 'MMMM MUCH TASTE', @Fruit),
('Kiwi', 'MMMM MUCH TASTE', @Fruit),
('Passionfruit', 'MMMM MUCH TASTE', @Fruit),
('Blueberries', 'MMMM MUCH TASTE', @Fruit),
('Blackberry', 'MMMM MUCH TASTE', @Fruit),
('Raspberry', 'MMMM MUCH TASTE', @Fruit),
('Watermelon', 'MMMM MUCH TASTE', @Fruit),
('Melon', 'MMMM MUCH TASTE', @Fruit),
('Tomatoe', 'MMMM MUCH TASTE', @Fruit),
('Advocado', 'MMMM MUCH TASTE', @Fruit),


--Legumes
('Tofu', 'MMMM NOT MUCH TASTE', @Legume),
('Green Pea', 'MMMM MUCH TASTE', @Legume),
('Green Bean', 'MMMM MUCH TASTE', @Legume),
('Chickpea', 'MMMM MUCH TASTE', @Legume),
('Soybean', 'MMMM MUCH TASTE', @Legume),
('Butter Bean', 'MMMM MUCH TASTE', @Legume),
('Broad Bean', 'MMMM MUCH TASTE', @Legume),


-- Pottery
('Tableware', 'Pots and cups, what would we do without pots and cups', @Pottery),
('Basic Religious Statue', 'Pots and cups, what would we do without pots and cups', @Pottery),

-- Houseware
('Wooden Chair', 'A basic wooden chair', @Houseware),
('Wooden Table', 'A basic wooden table', @Houseware),
('Wooden Chest', 'A basic wooden chest', @Houseware),
('Religious Ornament', 'A basic religious ornament', @Houseware),
('Religious Statue', 'A basic religious statue', @Houseware),


--  Leather
('Leather', 'Leather pieces for crafting armour or other items', 17)

-- add armour
-- add refined-stone(bricks, tiles etc)
-- add saddles 
-- add leather and leather stuff
-- add gold, silve, platinum items
-- add refined foods
-- add waste products
-- add castle defence items, balistas etc
-- add oil, oil products
-- add drugs, booze etc


;
end

go
begin
insert into res_actions(res_action, res_action_desc)
values
('Build', 'Resources required to construct this building'),
('Required', 'Resources required to generate desired resource'),
('Generated', 'Resources generated by building if requirements are met')

;
end

GO
BEGIN
declare @Iron int = (select id from resources where res_name = 'Iron')
declare @Copper int = (select id from resources where res_name = 'Copper')
declare @Gold int = (select id from resources where res_name = 'Gold')
declare @Silver int = (select id from resources where res_name = 'Silver')
declare @Mythril int = (select id from resources where res_name = 'Mythril')
declare @Platinum int = (select id from resources where res_name = 'Platinum')
declare @Aluminium int = (select id from resources where res_name = 'Aluminium')
declare @Tin int = (select id from resources where res_name = 'Tin')
declare @Diamond int = (select id from resources where res_name = 'Diamond')
declare @Zinc int = (select id from resources where res_name = 'Zinc')
declare @Ruby int = (select id from resources where res_name = 'Ruby')

-- Woods & Refined Woods
declare @Wood int = (select id from resources where res_name = 'Wood')
declare @Hardwood int = (select id from resources where res_name = 'Hardwood')
declare @Bamboo int = (select id from resources where res_name = 'Bamboo')

-- Stones
declare @Stone int = (select id from resources where res_name = 'Stone')
declare @Granite int = (select id from resources where res_name = 'Granite')
declare @Marble int = (select id from resources where res_name = 'Marble')
declare @Onyx int = (select id from resources where res_name = 'Onyx')
declare @Slate int = (select id from resources where res_name = 'Slate')


-- Clays
declare @CommonClay int = (select id from resources where res_name = 'Common Clay')
declare @StonewareClay int = (select id from resources where res_name = 'Stoneware Clay')
declare @KaolinClay int = (select id from resources where res_name = 'Kaolin Clay')


-- Vegetables
declare @Potatoe int = (select id from resources where res_name = 'Potatoe')
declare @Spinach int = (select id from resources where res_name = 'Spinach')
declare @Cabbage int = (select id from resources where res_name = 'Cabbage')
declare @Broccoli int = (select id from resources where res_name = 'Broccoli')
declare @Pumpkin int = (select id from resources where res_name = 'Pumpkin')
declare @SweetPotatoe int = (select id from resources where res_name = 'Sweet Potatoe')
declare @Asparagus int = (select id from resources where res_name = 'Asparagus')
declare @Onion int = (select id from resources where res_name = 'Onion')
declare @Garlic int = (select id from resources where res_name = 'Garlic')
declare @Cauliflower int = (select id from resources where res_name = 'Cauliflower')
declare @Zucchini int = (select id from resources where res_name = 'Zucchini')
declare @Lettuce int = (select id from resources where res_name = 'Lettuce')
declare @Cucumber int = (select id from resources where res_name = 'Cucumber')


-- Fruit
declare @Strawberry int = (select id from resources where res_name = 'Strawberry')
declare @Apple int = (select id from resources where res_name = 'Apple')
declare @Orange int = (select id from resources where res_name = 'Orange')
declare @Grapefruit int = (select id from resources where res_name = 'Grapefruit')
declare @Mandarin int = (select id from resources where res_name = 'Mandarin')
declare @Lime int = (select id from resources where res_name = 'Lime')
declare @Lemon int = (select id from resources where res_name = 'Lemon')
declare @Nectarine int = (select id from resources where res_name = 'Nectarine')
declare @Apricot int = (select id from resources where res_name = 'Apricot')
declare @Peaches int = (select id from resources where res_name = 'Peaches')
declare @Banana int = (select id from resources where res_name = 'Banana')
declare @Mango int = (select id from resources where res_name = 'Mango')
declare @Pear int = (select id from resources where res_name = 'Pear')
declare @Plum int = (select id from resources where res_name = 'Plum')
declare @Kiwi int = (select id from resources where res_name = 'Kiwi')
declare @Passionfruit int = (select id from resources where res_name = 'Passionfruit')
declare @Blueberries int = (select id from resources where res_name = 'Blueberries')
declare @Blackberry int = (select id from resources where res_name = 'Blackberry')
declare @Raspberry int = (select id from resources where res_name = 'Raspberry')
declare @Watermelon int = (select id from resources where res_name = 'Watermelon')
declare @Melon int = (select id from resources where res_name = 'Melon')
declare @Tomatoe int = (select id from resources where res_name = 'Tomatoe')
declare @Advocado int = (select id from resources where res_name = 'Advocado')


--Legumes
declare @Tofu int = (select id from resources where res_name = 'Tofu')
declare @GreenPea int = (select id from resources where res_name = 'Green Pea')
declare @GreenBean int = (select id from resources where res_name = 'Green Bean')
declare @Chickpea int = (select id from resources where res_name = 'Chickpea')
declare @Soybean int = (select id from resources where res_name = 'Soybean')
declare @ButterBean int = (select id from resources where res_name = 'Butter Bean')
declare @BroadBean int = (select id from resources where res_name = 'Broad Bean')

declare @Ice int = (select id from biomes where biome_name = 'Polar')
declare @Tundra int = (select id from biomes where biome_name = 'Tundra')
declare @Taiga int = (select id from biomes where biome_name = 'Boreal Forests')
declare @MedForest int = (select id from biomes where biome_name = 'Mediterranean Forests')
declare @Grassland int = (select id from biomes where biome_name = 'Temperate Grasslands')
declare @TemperateForest int = (select id from biomes where biome_name = 'Temperate Broadleaf')
declare @TropicalRainforest int = (select id from biomes where biome_name = 'Tropical Moist Forests')
declare @TemperateGiantRainforest int = (select id from biomes where biome_name = 'Temperate Conifer Forests')
declare @Savanna int = (select id from biomes where biome_name = 'Tropical Grasslands')
declare @Steppe int = (select id from biomes where biome_name = 'Montane')
declare @Alpine int = (select id from biomes where biome_name = 'Mountain')


insert into res_biomes(resid, biomeid, spawn_chance, min_spawn_amount, max_spawn_amount)
VALUES
-- ore 
(@Iron, @Ice, 0.8, 25000, 200000),
(@Iron, @Tundra, 0.8, 25000, 200000),
(@Iron, @Alpine, 0.8, 25000, 200000),
(@Iron, @Taiga, 0.8, 25000, 200000),
(@Iron, @Grassland, 0.4, 10000, 50000),
(@Iron, @TemperateForest, 0.6, 20000, 100000),
(@Iron, @TropicalRainforest, 0.2, 10000, 50000),
(@Iron, @TemperateGiantRainforest, 0.4, 10000, 50000),
(@Iron, @Savanna, 0.2, 10000, 50000),
(@Iron, @Steppe, 0.6, 20000, 100000),
(@Iron, @MedForest, 0.6, 20000, 100000),

(@Zinc, @Ice, 0.8, 25000, 200000),
(@Zinc, @Tundra, 0.8, 25000, 200000),
(@Zinc, @Alpine, 0.8, 25000, 200000),
(@Zinc, @Taiga, 0.8, 25000, 200000),
(@Zinc, @Grassland, 0.4, 10000, 50000),
(@Zinc, @TemperateForest, 0.6, 20000, 100000),
(@Zinc, @TropicalRainforest, 0.2, 10000, 50000),
(@Zinc, @TemperateGiantRainforest, 0.4, 10000, 50000),
(@Zinc, @Savanna, 0.2, 10000, 50000),
(@Zinc, @Steppe, 0.6, 20000, 100000),
(@Zinc, @MedForest, 0.6, 20000, 100000),

(@Copper, @Ice, 0.8, 25000, 200000),
(@Copper, @Tundra, 0.8, 25000, 200000),
(@Copper, @Alpine, 0.8, 25000, 200000),
(@Copper, @Taiga, 0.8, 25000, 200000),
(@Copper, @Grassland, 0.4, 10000, 50000),
(@Copper, @TemperateForest, 0.6, 20000, 100000),
(@Copper, @TropicalRainforest, 0.2, 10000, 50000),
(@Copper, @TemperateGiantRainforest, 0.4, 10000, 50000),
(@Copper, @Savanna, 0.2, 10000, 50000),
(@Copper, @Steppe, 0.6, 20000, 100000),
(@Copper, @MedForest, 0.6, 20000, 100000),

(@Gold, @Ice, 0.1, 2500, 20000),
(@Gold, @Tundra, 0.1, 2500, 20000),
(@Gold, @Alpine, 0.1, 2500, 20000),
(@Gold, @Taiga, 0.1, 2500, 20000),
(@Gold, @Grassland, 0.1, 1000, 50000),
(@Gold, @TemperateForest, 0.1, 2000, 10000),
(@Gold, @TropicalRainforest, 0.1, 1000, 15000),
(@Gold, @TemperateGiantRainforest, 0.1, 1000, 25000),
(@Gold, @Savanna, 0.1, 1000, 25000),
(@Gold, @Steppe, 0.1, 2000, 10000),
(@Gold, @MedForest, 0.1, 2000, 10000),

(@Silver, @Ice, 0.2, 2500, 20000),
(@Silver, @Tundra, 0.2, 2500, 20000),
(@Silver, @Alpine, 0.2, 2500, 20000),
(@Silver, @Taiga, 0.2, 2500, 20000),
(@Silver, @Grassland, 0.2, 2000, 50000),
(@Silver, @TemperateForest, 0.2, 2000, 10000),
(@Silver, @TropicalRainforest, 0.2, 1000, 15000),
(@Silver, @TemperateGiantRainforest, 0.2, 1000, 25000),
(@Silver, @Savanna, 0.2, 1000, 25000),
(@Silver, @Steppe, 0.2, 2000, 10000),
(@Silver, @MedForest, 0.2, 2000, 10000),

(@Mythril, @Ice, 0.1, 250, 2000),
(@Mythril, @Tundra, 0.1, 250, 2000),
(@Mythril, @Alpine, 0.1, 250, 2000),
(@Mythril, @Taiga, 0.1, 250, 2000),
(@Mythril, @Grassland, 0.1, 100, 500),
(@Mythril, @TemperateForest, 0.1, 200, 1000),
(@Mythril, @TropicalRainforest, 0.1, 100, 1500),
(@Mythril, @TemperateGiantRainforest, 0.1, 1000, 2500),
(@Mythril, @Savanna, 0.1, 1000, 2500),
(@Mythril, @Steppe, 0.1, 200, 1000),
(@Mythril, @MedForest, 0.1, 200, 1000),

(@Platinum, @Ice, 0.1, 250, 2000),
(@Platinum, @Tundra, 0.1, 250, 2000),
(@Platinum, @Alpine, 0.1, 250, 2000),
(@Platinum, @Taiga, 0.1, 250, 2000),
(@Platinum, @Grassland, 0.1, 100, 500),
(@Platinum, @TemperateForest, 0.1, 200, 1000),
(@Platinum, @TropicalRainforest, 0.1, 100, 1500),
(@Platinum, @TemperateGiantRainforest, 0.1, 1000, 2500),
(@Platinum, @Savanna, 0.1, 1000, 2500),
(@Platinum, @Steppe, 0.1, 200, 1000),
(@Platinum, @MedForest, 0.1, 200, 1000),

(@Aluminium, @Ice, 0.8, 25000, 200000),
(@Aluminium, @Tundra, 0.8, 25000, 200000),
(@Aluminium, @Alpine, 0.8, 25000, 200000),
(@Aluminium, @Taiga, 0.8, 25000, 200000),
(@Aluminium, @Grassland, 0.4, 10000, 50000),
(@Aluminium, @TemperateForest, 0.6, 20000, 100000),
(@Aluminium, @TropicalRainforest, 0.2, 10000, 50000),
(@Aluminium, @TemperateGiantRainforest, 0.4, 10000, 50000),
(@Aluminium, @Savanna, 0.2, 10000, 50000),
(@Aluminium, @Steppe, 0.6, 20000, 100000),
(@Aluminium, @MedForest, 0.6, 20000, 100000),

(@Tin, @Ice, 0.8, 25000, 200000),
(@Tin, @Tundra, 0.8, 25000, 200000),
(@Tin, @Alpine, 0.8, 25000, 200000),
(@Tin, @Taiga, 0.8, 25000, 200000),
(@Tin, @Grassland, 0.4, 10000, 50000),
(@Tin, @TemperateForest, 0.6, 20000, 100000),
(@Tin, @TropicalRainforest, 0.2, 10000, 50000),
(@Tin, @TemperateGiantRainforest, 0.4, 10000, 50000),
(@Tin, @Savanna, 0.2, 10000, 50000),
(@Tin, @Steppe, 0.6, 20000, 100000),
(@Tin, @MedForest, 0.6, 20000, 100000),

(@Diamond, @Ice, 0.1, 2, 20),
(@Diamond, @Tundra, 0.1, 2, 20),
(@Diamond, @Alpine, 0.1, 2, 20),
(@Diamond, @Taiga, 0.1, 2, 20),
(@Diamond, @Grassland, 0.1, 10, 50),
(@Diamond, @TemperateForest, 0.1, 20, 25),
(@Diamond, @TropicalRainforest, 0.1, 1, 15),
(@Diamond, @TemperateGiantRainforest, 0.1, 1, 25),
(@Diamond, @Savanna, 0.1, 10, 25),
(@Diamond, @Steppe, 0.1, 2, 10),
(@Diamond, @MedForest, 0.1, 2, 10),

(@Ruby, @Ice, 0.1, 2, 20),
(@Ruby, @Tundra, 0.1, 2, 20),
(@Ruby, @Alpine, 0.1, 2, 20),
(@Ruby, @Taiga, 0.1, 2, 20),
(@Ruby, @Grassland, 0.1, 10, 50),
(@Ruby, @TemperateForest, 0.1, 20, 25),
(@Ruby, @TropicalRainforest, 0.1, 1, 15),
(@Ruby, @TemperateGiantRainforest, 0.1, 1, 25),
(@Ruby, @Savanna, 0.1, 10, 25),
(@Ruby, @Steppe, 0.1, 2, 10),
(@Ruby, @MedForest, 0.1, 2, 10),


-- wood
(@Wood, @Taiga, 1, 250000, 500000),
(@Wood, @Grassland, 1, 10000, 25000),
(@Wood, @TemperateForest, 1, 250000, 500000),
(@Wood, @MedForest, 1, 250000, 500000),
(@Wood, @TropicalRainforest, 1, 500000, 1000000),
(@Wood, @TemperateGiantRainforest, 1, 500000, 1000000),
(@Wood, @Savanna, 1, 10000, 25000),
(@Wood, @Steppe, 1, 10000, 25000),


(@Hardwood, @Grassland, 1, 10000, 25000),
(@Hardwood, @TemperateForest, 1, 250000, 500000),
(@Hardwood, @TropicalRainforest, 1, 500000, 1000000),
(@Hardwood, @TemperateGiantRainforest, 1, 500000, 1000000),
(@Hardwood, @MedForest, 1, 250000, 500000),

(@Bamboo, @TropicalRainforest, 1, 500000, 1000000),



-- stones
(@Stone, @Ice, 1, 25000, 200000),
(@Stone, @Tundra, 1, 10000000, 100000000),
(@Stone, @Alpine, 1, 10000000, 100000000),
(@Stone, @Taiga, 1, 250000, 2000000),
(@Stone, @Grassland, 1, 10000, 50000),
(@Stone, @TemperateForest, 1, 200000, 1000000),
(@Stone, @MedForest, 1, 200000, 1000000),
(@Stone, @TropicalRainforest, 1, 100000, 500000),
(@Stone, @TemperateGiantRainforest, 1, 100000, 500000),
(@Stone, @Savanna, 1, 100000, 500000),
(@Stone, @Steppe, 1, 200000, 1000000),


(@Granite, @Ice, 1, 25000, 200000),
(@Granite, @Tundra, 1, 10000000, 100000000),
(@Granite, @Alpine, 1, 10000000, 100000000),
(@Granite, @Taiga, 1, 250000, 2000000),
(@Granite, @Grassland, 1, 10000, 50000),
(@Granite, @TemperateForest, 1, 200000, 1000000),
(@Granite, @MedForest, 1, 200000, 1000000),
(@Granite, @TropicalRainforest, 1, 100000, 500000),
(@Granite, @TemperateGiantRainforest, 1, 100000, 500000),
(@Granite, @Savanna, 1, 100000, 500000),
(@Granite, @Steppe, 1, 200000, 1000000),

(@Marble, @Ice, 0.1, 2500, 20000),
(@Marble, @Tundra, 0.1, 10000, 100000),
(@Marble, @Alpine, 0.1, 10000, 100000),
(@Marble, @Taiga, 0.1, 10000, 100000),
(@Marble, @Grassland, 0.1, 10000, 100000),
(@Marble, @TemperateForest, 0.1, 10000, 100000),
(@Marble, @MedForest, 0.1, 10000, 100000),
(@Marble, @TropicalRainforest, 0.1, 10000, 100000),
(@Marble, @TemperateGiantRainforest, 0.1, 10000, 100000),
(@Marble, @Savanna, 0.1, 10000, 100000),
(@Marble, @Steppe, 0.1, 10000, 100000),

(@Onyx, @Ice, 0.1, 2500, 20000),
(@Onyx, @Tundra, 0.1, 10000, 100000),
(@Onyx, @Alpine, 0.1, 10000, 100000),
(@Onyx, @Taiga, 0.1, 10000, 100000),
(@Onyx, @Grassland, 0.1, 10000, 100000),
(@Onyx, @TemperateForest, 0.1, 10000, 100000),
(@Onyx, @MedForest, 0.1, 10000, 100000),
(@Onyx, @TropicalRainforest, 0.1, 10000, 100000),
(@Onyx, @TemperateGiantRainforest, 0.1, 10000, 100000),
(@Onyx, @Savanna, 0.1, 10000, 100000),
(@Onyx, @Steppe, 0.1, 10000, 100000),

(@Slate, @Ice, 1, 25000, 200000),
(@Slate, @Tundra, 1, 10000000, 100000000),
(@Slate, @Alpine, 1, 10000000, 100000000),
(@Slate, @Taiga, 1, 250000, 2000000),
(@Slate, @Grassland, 1, 10000, 50000),
(@Slate, @TemperateForest, 1, 200000, 1000000),
(@Slate, @MedForest, 1, 200000, 1000000),
(@Slate, @TropicalRainforest, 1, 100000, 500000),
(@Slate, @TemperateGiantRainforest, 1, 100000, 500000),
(@Slate, @Savanna, 1, 100000, 500000),
(@Slate, @Steppe, 1, 200000, 1000000),



-- clays
(@CommonClay, @Taiga, 1, 25000, 200000),
(@CommonClay, @Grassland, 1, 1000000, 5000000),
(@CommonClay, @TemperateForest, 1, 250000, 500000),
(@CommonClay, @MedForest, 1, 250000, 500000),
(@CommonClay, @TropicalRainforest, 1, 2000000, 7000000),
(@CommonClay, @TemperateGiantRainforest, 1, 1000000, 5000000),
(@CommonClay, @Savanna, 1, 1000000, 5000000),
(@CommonClay, @Steppe, 1, 1000000, 5000000),

(@StonewareClay, @Taiga, 0.5, 25000, 200000),
(@StonewareClay, @Grassland, 0.5, 1000000, 5000000),
(@StonewareClay, @TemperateForest, 0.5, 250000, 500000),
(@StonewareClay, @MedForest, 0.5, 250000, 500000),
(@StonewareClay, @TropicalRainforest, 0.5, 2000000, 7000000),
(@StonewareClay, @TemperateGiantRainforest, 0.5, 1000000, 5000000),
(@StonewareClay, @Savanna, 0.5, 1000000, 5000000),
(@StonewareClay, @Steppe, 0.5, 1000000, 5000000),

(@KaolinClay, @Taiga, 0.1, 2500, 20000),
(@KaolinClay, @Grassland, 0.1, 100000, 500000),
(@KaolinClay, @TemperateForest, 0.1, 25000, 50000),
(@KaolinClay, @MedForest, 0.1, 25000, 50000),
(@KaolinClay, @TropicalRainforest, 0.1, 200000, 700000),
(@KaolinClay, @TemperateGiantRainforest, 0.1, 100000, 500000),
(@KaolinClay, @Savanna, 0.1, 100000, 500000),
(@KaolinClay, @Steppe, 0.1, 100000, 500000),



-- vegetables
(@Potatoe, @Taiga, 1, 100, 5000),
(@Potatoe, @Grassland, 1, 1000, 15000),
(@Potatoe, @TemperateForest, 1, 1000, 6000),
(@Potatoe, @MedForest, 1, 1000, 6000),
(@Potatoe, @TropicalRainforest, 1, 50000, 100000),
(@Potatoe, @TemperateGiantRainforest, 1, 1000, 6000),
(@Potatoe, @Savanna, 1, 1000, 6000),
(@Potatoe, @Steppe, 1, 1000, 6000),

(@Spinach, @Taiga, 0.1, 100, 5000),
(@Spinach, @Grassland, 0.1, 1000, 15000),
(@Spinach, @TemperateForest, 0.1, 1000, 6000),
(@Spinach, @MedForest, 0.1, 1000, 6000),
(@Spinach, @TropicalRainforest, 0.1, 50000, 100000),
(@Spinach, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Spinach, @Savanna, 0.1, 1000, 6000),
(@Spinach, @Steppe, 0.1, 1000, 6000),

(@Cabbage, @Taiga, 0.1, 100, 5000),
(@Cabbage, @Grassland, 0.1, 1000, 15000),
(@Cabbage, @TemperateForest, 0.1, 1000, 6000),
(@Cabbage, @MedForest, 0.1, 1000, 6000),
(@Cabbage, @TropicalRainforest, 0.1, 50000, 100000),
(@Cabbage, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Cabbage, @Savanna, 0.1, 1000, 6000),
(@Cabbage, @Steppe, 0.1, 1000, 6000),

(@Broccoli, @Taiga, 0.1, 100, 5000),
(@Broccoli, @Grassland, 0.1, 1000, 15000),
(@Broccoli, @TemperateForest, 0.1, 1000, 6000),
(@Broccoli, @MedForest, 0.1, 1000, 6000),
(@Broccoli, @TropicalRainforest, 0.1, 50000, 100000),
(@Broccoli, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Broccoli, @Savanna, 0.1, 1000, 6000),
(@Broccoli, @Steppe, 0.1, 1000, 6000),

(@Pumpkin, @Taiga, 0.1, 100, 5000),
(@Pumpkin, @Grassland, 0.1, 1000, 15000),
(@Pumpkin, @TemperateForest, 0.1, 1000, 6000),
(@Pumpkin, @MedForest, 0.1, 1000, 6000),
(@Pumpkin, @TropicalRainforest, 0.1, 50000, 100000),
(@Pumpkin, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Pumpkin, @Savanna, 0.1, 1000, 6000),
(@Pumpkin, @Steppe, 0.1, 1000, 6000),

(@SweetPotatoe, @Taiga, 0.1, 100, 5000),
(@SweetPotatoe, @Grassland, 0.1, 1000, 15000),
(@SweetPotatoe, @TemperateForest, 0.1, 1000, 6000),
(@SweetPotatoe, @MedForest, 0.1, 1000, 6000),
(@SweetPotatoe, @TropicalRainforest, 0.1, 50000, 100000),
(@SweetPotatoe, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@SweetPotatoe, @Savanna, 0.1, 1000, 6000),
(@SweetPotatoe, @Steppe, 0.1, 1000, 6000),

(@Asparagus , @Taiga, 0.1, 100, 5000),
(@Asparagus , @Grassland, 0.1, 1000, 15000),
(@Asparagus , @TemperateForest, 0.1, 1000, 6000),
(@Asparagus , @MedForest, 0.1, 1000, 6000),
(@Asparagus , @TropicalRainforest, 0.1, 50000, 100000),
(@Asparagus , @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Asparagus , @Savanna, 0.1, 1000, 6000),
(@Asparagus , @Steppe, 0.1, 1000, 6000),

(@Onion, @Taiga, 0.1, 100, 5000),
(@Onion, @Grassland, 0.1, 1000, 15000),
(@Onion, @TemperateForest, 0.1, 1000, 6000),
(@Onion, @MedForest, 0.1, 1000, 6000),
(@Onion, @TropicalRainforest, 0.1, 50000, 100000),
(@Onion, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Onion, @Savanna, 0.1, 1000, 6000),
(@Onion, @Steppe, 0.1, 1000, 6000),

(@Garlic, @Taiga, 0.1, 100, 5000),
(@Garlic, @Grassland, 0.1, 1000, 15000),
(@Garlic, @TemperateForest, 0.1, 1000, 6000),
(@Garlic, @MedForest, 0.1, 1000, 6000),
(@Garlic, @TropicalRainforest, 0.1, 50000, 100000),
(@Garlic, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Garlic, @Savanna, 0.1, 1000, 6000),
(@Garlic, @Steppe, 0.1, 1000, 6000),

(@Cauliflower, @Taiga, 0.1, 100, 5000),
(@Cauliflower, @Grassland, 0.1, 1000, 15000),
(@Cauliflower, @TemperateForest, 0.1, 1000, 6000),
(@Cauliflower, @MedForest, 0.1, 1000, 6000),
(@Cauliflower, @TropicalRainforest, 0.1, 50000, 100000),
(@Cauliflower, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Cauliflower, @Savanna, 0.1, 1000, 6000),
(@Cauliflower, @Steppe, 0.1, 1000, 6000),

(@Zucchini, @Taiga, 0.1, 100, 5000),
(@Zucchini, @Grassland, 0.1, 1000, 15000),
(@Zucchini, @TemperateForest, 0.1, 1000, 6000),
(@Zucchini, @MedForest, 0.1, 1000, 6000),
(@Zucchini, @TropicalRainforest, 0.1, 50000, 100000),
(@Zucchini, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Zucchini, @Savanna, 0.1, 1000, 6000),
(@Zucchini, @Steppe, 0.1, 1000, 6000),

(@Lettuce, @Taiga, 0.1, 100, 5000),
(@Lettuce, @Grassland, 0.1, 1000, 15000),
(@Lettuce, @TemperateForest, 0.1, 1000, 6000),
(@Lettuce, @MedForest, 0.1, 1000, 6000),
(@Lettuce, @TropicalRainforest, 0.1, 50000, 100000),
(@Lettuce, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Lettuce, @Savanna, 0.1, 1000, 6000),
(@Lettuce, @Steppe, 0.1, 1000, 6000),

(@Cucumber, @Taiga, 0.1, 100, 5000),
(@Cucumber, @Grassland, 0.1, 1000, 15000),
(@Cucumber, @TemperateForest, 0.1, 1000, 6000),
(@Cucumber, @MedForest, 0.1, 1000, 6000),
(@Cucumber, @TropicalRainforest, 0.1, 50000, 100000),
(@Cucumber, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Cucumber, @Savanna, 0.1, 1000, 6000),
(@Cucumber, @Steppe, 0.1, 1000, 6000),

(@Strawberry, @Taiga, 0.1, 100, 5000),
(@Strawberry, @Grassland, 0.1, 1000, 15000),
(@Strawberry, @TemperateForest, 0.1, 1000, 6000),
(@Strawberry, @MedForest, 0.1, 1000, 6000),
(@Strawberry, @TropicalRainforest, 0.1, 50000, 100000),
(@Strawberry, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Strawberry, @Savanna, 0.1, 1000, 6000),
(@Strawberry, @Steppe, 0.1, 1000, 6000),

(@Apple, @Taiga, 0.1, 100, 5000),
(@Apple, @Grassland, 0.1, 1000, 15000),
(@Apple, @TemperateForest, 0.1, 1000, 6000),
(@Apple, @MedForest, 0.1, 1000, 6000),
(@Apple, @TropicalRainforest, 0.1, 50000, 100000),
(@Apple, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Apple, @Savanna, 0.1, 1000, 6000),
(@Apple, @Steppe, 0.1, 1000, 6000),

(@Orange, @Taiga, 0.1, 100, 5000),
(@Orange, @Grassland, 0.1, 1000, 15000),
(@Orange, @TemperateForest, 0.1, 1000, 6000),
(@Orange, @MedForest, 0.1, 1000, 6000),
(@Orange, @TropicalRainforest, 0.1, 50000, 100000),
(@Orange, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Orange, @Savanna, 0.1, 1000, 6000),
(@Orange, @Steppe, 0.1, 1000, 6000),

(@Grapefruit, @Taiga, 0.1, 100, 5000),
(@Grapefruit, @Grassland, 0.1, 1000, 15000),
(@Grapefruit, @TemperateForest, 0.1, 1000, 6000),
(@Grapefruit, @MedForest, 0.1, 1000, 6000),
(@Grapefruit, @TropicalRainforest, 0.1, 50000, 100000),
(@Grapefruit, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Grapefruit, @Savanna, 0.1, 1000, 6000),
(@Grapefruit, @Steppe, 0.1, 1000, 6000),

(@Mandarin, @Taiga, 0.1, 100, 5000),
(@Mandarin, @Grassland, 0.1, 1000, 15000),
(@Mandarin, @TemperateForest, 0.1, 1000, 6000),
(@Mandarin, @MedForest, 0.1, 1000, 6000),
(@Mandarin, @TropicalRainforest, 0.1, 50000, 100000),
(@Mandarin, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Mandarin, @Savanna, 0.1, 1000, 6000),
(@Mandarin, @Steppe, 0.1, 1000, 6000),

(@Lime, @Taiga, 0.1, 100, 5000),
(@Lime, @Grassland, 0.1, 1000, 15000),
(@Lime, @TemperateForest, 0.1, 1000, 6000),
(@Lime, @MedForest, 0.1, 1000, 6000),
(@Lime, @TropicalRainforest, 0.1, 50000, 100000),
(@Lime, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Lime, @Savanna, 0.1, 1000, 6000),
(@Lime, @Steppe, 0.1, 1000, 6000),

(@Lemon, @Taiga, 0.1, 100, 5000),
(@Lemon, @Grassland, 0.1, 1000, 15000),
(@Lemon, @TemperateForest, 0.1, 1000, 6000),
(@Lemon, @MedForest, 0.1, 1000, 6000),
(@Lemon, @TropicalRainforest, 0.1, 50000, 100000),
(@Lemon, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Lemon, @Savanna, 0.1, 1000, 6000),
(@Lemon, @Steppe, 0.1, 1000, 6000),

(@Nectarine, @Taiga, 0.1, 100, 5000),
(@Nectarine, @Grassland, 0.1, 1000, 15000),
(@Nectarine, @TemperateForest, 0.1, 1000, 6000),
(@Nectarine, @MedForest, 0.1, 1000, 6000),
(@Nectarine, @TropicalRainforest, 0.1, 50000, 100000),
(@Nectarine, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Nectarine, @Savanna, 0.1, 1000, 6000),
(@Nectarine, @Steppe, 0.1, 1000, 6000),

(@Apricot, @Taiga, 0.1, 100, 5000),
(@Apricot, @Grassland, 0.1, 1000, 15000),
(@Apricot, @TemperateForest, 0.1, 1000, 6000),
(@Apricot, @MedForest, 0.1, 1000, 6000),
(@Apricot, @TropicalRainforest, 0.1, 50000, 100000),
(@Apricot, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Apricot, @Savanna, 0.1, 1000, 6000),
(@Apricot, @Steppe, 0.1, 1000, 6000),

(@Peaches, @Taiga, 0.1, 100, 5000),
(@Peaches, @Grassland, 0.1, 1000, 15000),
(@Peaches, @TemperateForest, 0.1, 1000, 6000),
(@Peaches, @MedForest, 0.1, 1000, 6000),
(@Peaches, @TropicalRainforest, 0.1, 50000, 100000),
(@Peaches, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Peaches, @Savanna, 0.1, 1000, 6000),
(@Peaches, @Steppe, 0.1, 1000, 6000),

(@Banana, @Taiga, 0.1, 100, 5000),
(@Banana, @Grassland, 0.1, 1000, 15000),
(@Banana, @TemperateForest, 0.1, 1000, 6000),
(@Banana, @MedForest, 0.1, 1000, 6000),
(@Banana, @TropicalRainforest, 0.1, 50000, 100000),
(@Banana, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Banana, @Savanna, 0.1, 1000, 6000),
(@Banana, @Steppe, 0.1, 1000, 6000),

(@Mango, @Taiga, 0.1, 100, 5000),
(@Mango, @Grassland, 0.1, 1000, 15000),
(@Mango, @TemperateForest, 0.1, 1000, 6000),
(@Mango, @MedForest, 0.1, 1000, 6000),
(@Mango, @TropicalRainforest, 0.1, 50000, 100000),
(@Mango, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Mango, @Savanna, 0.1, 1000, 6000),
(@Mango, @Steppe, 0.1, 1000, 6000),

(@Pear, @Taiga, 0.1, 100, 5000),
(@Pear, @Grassland, 0.1, 1000, 15000),
(@Pear, @TemperateForest, 0.1, 1000, 6000),
(@Pear, @MedForest, 0.1, 1000, 6000),
(@Pear, @TropicalRainforest, 0.1, 50000, 100000),
(@Pear, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Pear, @Savanna, 0.1, 1000, 6000),
(@Pear, @Steppe, 0.1, 1000, 6000),

(@Plum, @Taiga, 0.1, 100, 5000),
(@Plum, @Grassland, 0.1, 1000, 15000),
(@Plum, @TemperateForest, 0.1, 1000, 6000),
(@Plum, @MedForest, 0.1, 1000, 6000),
(@Plum, @TropicalRainforest, 0.1, 50000, 100000),
(@Plum, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Plum, @Savanna, 0.1, 1000, 6000),
(@Plum, @Steppe, 0.1, 1000, 6000),

(@Kiwi, @Taiga, 0.1, 100, 5000),
(@Kiwi, @Grassland, 0.1, 1000, 15000),
(@Kiwi, @TemperateForest, 0.1, 1000, 6000),
(@Kiwi, @MedForest, 0.1, 1000, 6000),
(@Kiwi, @TropicalRainforest, 0.1, 50000, 100000),
(@Kiwi, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Kiwi, @Savanna, 0.1, 1000, 6000),
(@Kiwi, @Steppe, 0.1, 1000, 6000),

(@Passionfruit, @Taiga, 0.1, 100, 5000),
(@Passionfruit, @Grassland, 0.1, 1000, 15000),
(@Passionfruit, @TemperateForest, 0.1, 1000, 6000),
(@Passionfruit, @MedForest, 0.1, 1000, 6000),
(@Passionfruit, @TropicalRainforest, 0.1, 50000, 100000),
(@Passionfruit, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Passionfruit, @Savanna, 0.1, 1000, 6000),
(@Passionfruit, @Steppe, 0.1, 1000, 6000),

(@Blueberries, @Taiga, 0.1, 100, 5000),
(@Blueberries, @Grassland, 0.1, 1000, 15000),
(@Blueberries, @TemperateForest, 0.1, 1000, 6000),
(@Blueberries, @MedForest, 0.1, 1000, 6000),
(@Blueberries, @TropicalRainforest, 0.1, 50000, 100000),
(@Blueberries, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Blueberries, @Savanna, 0.1, 1000, 6000),
(@Blueberries, @Steppe, 0.1, 1000, 6000),

(@Blackberry, @Taiga, 0.1, 100, 5000),
(@Blackberry, @Grassland, 0.1, 1000, 15000),
(@Blackberry, @TemperateForest, 0.1, 1000, 6000),
(@Blackberry, @MedForest, 0.1, 1000, 6000),
(@Blackberry, @TropicalRainforest, 0.1, 50000, 100000),
(@Blackberry, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Blackberry, @Savanna, 0.1, 1000, 6000),
(@Blackberry, @Steppe, 0.1, 1000, 6000),

(@Raspberry, @Taiga, 0.1, 100, 5000),
(@Raspberry, @Grassland, 0.1, 1000, 15000),
(@Raspberry, @TemperateForest, 0.1, 1000, 6000),
(@Raspberry, @MedForest, 0.1, 1000, 6000),
(@Raspberry, @TropicalRainforest, 0.1, 50000, 100000),
(@Raspberry, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Raspberry, @Savanna, 0.1, 1000, 6000),
(@Raspberry, @Steppe, 0.1, 1000, 6000),

(@Watermelon, @Taiga, 0.1, 100, 5000),
(@Watermelon, @Grassland, 0.1, 1000, 15000),
(@Watermelon, @TemperateForest, 0.1, 1000, 6000),
(@Watermelon, @MedForest, 0.1, 1000, 6000),
(@Watermelon, @TropicalRainforest, 0.1, 50000, 100000),
(@Watermelon, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Watermelon, @Savanna, 0.1, 1000, 6000),
(@Watermelon, @Steppe, 0.1, 1000, 6000),

(@Melon, @Taiga, 0.1, 100, 5000),
(@Melon, @Grassland, 0.1, 1000, 15000),
(@Melon, @TemperateForest, 0.1, 1000, 6000),
(@Melon, @MedForest, 0.1, 1000, 6000),
(@Melon, @TropicalRainforest, 0.1, 50000, 100000),
(@Melon, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Melon, @Savanna, 0.1, 1000, 6000),
(@Melon, @Steppe, 0.1, 1000, 6000),

(@Tomatoe, @Taiga, 0.1, 100, 5000),
(@Tomatoe, @Grassland, 0.1, 1000, 15000),
(@Tomatoe, @TemperateForest, 0.1, 1000, 6000),
(@Tomatoe, @MedForest, 0.1, 1000, 6000),
(@Tomatoe, @TropicalRainforest, 0.1, 50000, 100000),
(@Tomatoe, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Tomatoe, @Savanna, 0.1, 1000, 6000),
(@Tomatoe, @Steppe, 0.1, 1000, 6000),

(@Advocado, @Taiga, 0.1, 100, 5000),
(@Advocado, @Grassland, 0.1, 1000, 15000),
(@Advocado, @TemperateForest, 0.1, 1000, 6000),
(@Advocado, @MedForest, 0.1, 1000, 6000),
(@Advocado, @TropicalRainforest, 0.1, 50000, 100000),
(@Advocado, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Advocado, @Savanna, 0.1, 1000, 6000),
(@Advocado, @Steppe, 0.1, 1000, 6000),

-- legumes
(@Tofu, @Taiga, 0.1, 100, 5000),
(@Tofu, @Grassland, 0.1, 1000, 15000),
(@Tofu, @TemperateForest, 0.1, 1000, 6000),
(@Tofu, @MedForest, 0.1, 1000, 6000),
(@Tofu, @TropicalRainforest, 0.1, 50000, 100000),
(@Tofu, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Tofu, @Savanna, 0.1, 1000, 6000),
(@Tofu, @Steppe, 0.1, 1000, 6000),

(@GreenPea, @Taiga, 0.1, 100, 5000),
(@GreenPea, @Grassland, 0.1, 1000, 15000),
(@GreenPea, @TemperateForest, 0.1, 1000, 6000),
(@GreenPea, @MedForest, 0.1, 1000, 6000),
(@GreenPea, @TropicalRainforest, 0.1, 50000, 100000),
(@GreenPea, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@GreenPea, @Savanna, 0.1, 1000, 6000),
(@GreenPea, @Steppe, 0.1, 1000, 6000),

(@GreenBean, @Taiga, 0.1, 100, 5000),
(@GreenBean, @Grassland, 0.1, 1000, 15000),
(@GreenBean, @TemperateForest, 0.1, 1000, 6000),
(@GreenBean, @MedForest, 0.1, 1000, 6000),
(@GreenBean, @TropicalRainforest, 0.1, 50000, 100000),
(@GreenBean, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@GreenBean, @Savanna, 0.1, 1000, 6000),
(@GreenBean, @Steppe, 0.1, 1000, 6000),

(@Chickpea, @Taiga, 0.1, 100, 5000),
(@Chickpea, @Grassland, 0.1, 1000, 15000),
(@Chickpea, @TemperateForest, 0.1, 1000, 6000),
(@Chickpea, @MedForest, 0.1, 1000, 6000),
(@Chickpea, @TropicalRainforest, 0.1, 50000, 100000),
(@Chickpea, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Chickpea, @Savanna, 0.1, 1000, 6000),
(@Chickpea, @Steppe, 0.1, 1000, 6000),

(@Soybean, @Taiga, 0.1, 100, 5000),
(@Soybean, @Grassland, 0.1, 1000, 15000),
(@Soybean, @TemperateForest, 0.1, 1000, 6000),
(@Soybean, @MedForest, 0.1, 1000, 6000),
(@Soybean, @TropicalRainforest, 0.1, 50000, 100000),
(@Soybean, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@Soybean, @Savanna, 0.1, 1000, 6000),
(@Soybean, @Steppe, 0.1, 1000, 6000),

(@ButterBean, @Taiga, 0.1, 100, 5000),
(@ButterBean, @Grassland, 0.1, 1000, 15000),
(@ButterBean, @TemperateForest, 0.1, 1000, 6000),
(@ButterBean, @MedForest, 0.1, 1000, 6000),
(@ButterBean, @TropicalRainforest, 0.1, 50000, 100000),
(@ButterBean, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@ButterBean, @Savanna, 0.1, 1000, 6000),
(@ButterBean, @Steppe, 0.1, 1000, 6000),

(@BroadBean, @Taiga, 0.1, 100, 5000),
(@BroadBean, @Grassland, 0.1, 1000, 15000),
(@BroadBean, @TemperateForest, 0.1, 1000, 6000),
(@BroadBean, @MedForest, 0.1, 1000, 6000),
(@BroadBean, @TropicalRainforest, 0.1, 50000, 100000),
(@BroadBean, @TemperateGiantRainforest, 0.1, 1000, 6000),
(@BroadBean, @Savanna, 0.1, 1000, 6000),
(@BroadBean, @Steppe, 0.1, 1000, 6000)


;
end


go
BEGIN
declare @Iron int = (select id from resources where res_name = 'Refined Iron')
declare @Copper int = (select id from resources where res_name = 'Refined Copper')
declare @Wood int = (select id from resources where res_name = 'Timber')
declare @Hardwood int = (select id from resources where res_name = 'Hardwood Timber')
declare @Leather int = (select id from resources where res_name = 'Leather')
insert into instruments(instrument, resid)
VALUES
('Flute', @Wood),
('Rebec', @Iron),
('Harp', @Iron),
('Fiddle', @Iron),
('Psaltery', @Iron),
('Chittarone', @Copper),
('Cittern', @Iron),
('Dulcimer', @Copper),
('Gittern', @Iron),
('Viol', @Iron),
('Vielle', @Iron),
('Mandolin', @Iron),
('Harpsichord', @Copper),
('Spinet', @Iron),
('Trumpet', @Copper),
('Pipe', @Iron),
('Recorder', @Wood),
('Bagpipe', @Leather),
('Shawm', @Iron),
('Crumhorn', @Copper),
('Cornett', @Copper),
('Ocarina', @Hardwood),
('Sackbut', @Iron),
('Hautboy', @Iron),
('Horn', @Iron),
('Bombard', @Copper),
('Tuba', @Copper),
('Oboe', @Hardwood),
('Trombone', @Copper),
('Drum', @Leather),
('Bells', @Iron),
('Tabor', @Iron),
('Tambourine', @Iron),
('Pipe Organ', @Copper)
;
end