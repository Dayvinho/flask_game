from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from flask_game import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return users_tbl.query.get(int(user_id))


class users_tbl(db.Model, UserMixin):
    __tablename__ = 'users_tbl'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    passw = db.Column(db.String(60), nullable=False)
    gameid = db.Column(db.Integer)
    fp_posts = db.relationship('fp_post', backref='createdby')

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return users_tbl.query.get(user_id)

    def __repr__(self):
        return f"users_tbl('{self.username}', '{self.email}', '{self.image_file}')"


class fp_post(db.Model):
    __tablename__ = 'site_fp_posts'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    post = db.Column(db.Text, nullable=False)
    datecreated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    userid = db.Column(db.Integer, db.ForeignKey('users_tbl.id'), nullable=False)

    def __repr__(self):
        return f"fp_post('{self.title}', '{self.date_posted}')"


class game_maps(db.Model):
    __tablename__ = 'game_maps'
    id = db.Column(db.Integer, primary_key=True)
    game_name = db.Column(db.String(100), nullable=False, unique=True)
    input_x = db.Column(db.Integer, nullable=False)
    input_y = db.Column(db.Integer, nullable=False)
    act_x = db.Column(db.Integer, nullable=False)
    act_y = db.Column(db.Integer, nullable=False)
    datecreated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return f"game_maps('{self.game_name}', '{self.datecreated}', '{self.act_x}', '{self.act_y}')"


class game_biomes(db.Model):
    __tablename__ = 'game_biomes'
    id = db.Column(db.Integer, primary_key=True)
    biome_name = db.Column(db.String(50), nullable=False, unique=True)

    def __repr__(self):
        return f"game_biomes('{self.biome_name}')"


class game_maps_users(db.Model):
    __tablename__ = 'game_maps_users'
    id = db.Column(db.Integer, primary_key=True)
    userid = db.Column(db.Integer, db.ForeignKey('users_tbl.id'), nullable=False)
    mapid = db.Column(db.Integer, db.ForeignKey('game_maps.id'), nullable=False)
    datecreated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return f"game_maps_users('{self.userid}', '{self.mapid}', '{self.datecreated}')"


class game_map_data(db.Model):
    __tablename__ = 'game_map_data'
    id = db.Column(db.Integer, primary_key=True)
    mapid = db.Column(db.Integer, db.ForeignKey('game_maps.id'), nullable=False)
    x = db.Column(db.Integer)
    y = db.Column(db.Integer)
    z = db.Column(db.Integer)
    elevation = db.Column(db.Integer)
    equator_distance = db.Column(db.Integer)
    biomeid_a = db.Column(db.Integer, db.ForeignKey('game_biomes.id'), nullable=False)

    #def __repr__(self):
    #    return f"""{
    #        'mapid': {self.mapid}, 
    #        'x': {self.x}, 
    #        'y': {self.y}, 
    #        'z': {self.z},
    #        'elevation':  {self.elevation},
    #        'equator_distance': {self.equator_distance},
    #        'biomeid_a': {self.biomeid_a}
    #        }"""

