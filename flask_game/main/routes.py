from flask import render_template, request, Blueprint
from flask_game.models import fp_post

main = Blueprint('main', __name__)

@main.route("/")
@main.route("/home")
def home():
    page = request.args.get('page', 1, type=int)
    posts = fp_post.query.order_by(fp_post.datecreated.desc()).paginate(page=page, per_page=2)
    return render_template('home.html', posts=posts)

@main.route("/about")
def about():
    return render_template('about.html', title='About')