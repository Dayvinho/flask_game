SELECT HTML_DECODE('&lt;p&gt;The piano was made ' ||
  'by &lsquo;Steinway &amp; Sons&rsquo;.&lt;/p&gt;')



DROP VARIABLE x;
CREATE VARIABLE x XML;
SELECT html_decode(to_char(xp_read_file('D:\XML_Transfer\Meir\Results\Failed\INV-12669.xml')))
 INTO x;
SELECT * FROM openxml( x, '//*')
 WITH (ID INT '@mp:id',
       parent INT '../@mp:id',
       name CHAR(128) '@mp:localname',
       text LONG VARCHAR 'text()' )
ORDER BY ID;


DROP VARIABLE x;
CREATE VARIABLE x XML;
SELECT html_encode('<orders><order_new><order_no>INV-12669</order_no><order_date>16/05/2019</order_date><grand_total /><currency /><store_brand /><shipping_method>Retail Customer</shipping_method><cust_order /><note /><billing_address><first_name>Flagship</first_name><last_name>Construction Ltd</last_name><street_address_1 /><street_address_2 /><city /><state /><zip_code /><country /><telephone>+44 (0) 7985 116 060</telephone><email>kh@flagship.uk.com</email></billing_address><shipping_address><first_name>Kevin</first_name><last_name>Hyde</last_name><street_address_1>Flagship Construction Ltd Unit 3</street_address_1><street_address_2>Armitage Business Centre</street_address_2><city>Delamare Rd</city><state>Cheshunt. EN8 9FN</state><zip_code /><country /><telephone /><email /></shipping_address><order_items><items><item><id>1</id><product_name>Diverter Handle for MW07 Part 5&6 Matte Black</product_name><sku>DMW07-PART5 & 6</sku><qty>2.00</qty></item></items></order_items></order_new></orders>')
into x;
select * from openxml(x, '//*')
 WITH (ID INT '@mp:id',
       parent INT '../@mp:id',
       name CHAR(128) '@mp:localname',
       text LONG VARCHAR 'text()' )
ORDER BY ID;
