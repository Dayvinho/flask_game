from flask import render_template, Blueprint, url_for, redirect, flash
from flask_login import login_required, current_user
from flask_game.engine import conf, db_engine
from pygal.style import DarkStyle
import pandas as pd
import sys
import pickle
import os
import re
import pygal

game_routes = Blueprint('game_routes', __name__)

def remove_ids(df):
    del df['userid']
    del df['gameid']
    return df

@game_routes.route("/overview", methods=['POST', 'GET'])
@login_required
def overview():
    return render_template('overview.html')

@game_routes.route("/overview_peons", methods=['POST', 'GET'])
@login_required
def overview_peons():
    df_peon_info = pd.read_sql(f'''
        select * from game.vw_peon_info
        where userid = {current_user.id}
        and gameid = {current_user.gameid}
        ''', db_engine)
    df_peon_info = remove_ids(df_peon_info)
    peon_table = df_peon_info.to_html(classes='p-1 mt-1', 
                                table_id='peontable', 
                                justify='center', 
                                index=False, 
                                border=0)

    df_peons_agg = pd.read_sql(f'''
        select *
        from game.vw_player_peon_agg
        where userid = {current_user.id}
        and gameid = {current_user.gameid}
        ''', db_engine)
    df_peons_agg = remove_ids(df_peons_agg)

    peon_count = df_peons_agg.peon_count.item()
    happy_val = df_peons_agg.happy.item() * 100
    hunger_val = df_peons_agg.hunger.item() * 100
    thirst_val = df_peons_agg.thirst.item() * 100
    energy_val = df_peons_agg.energy.item() * 100
    lust_val = df_peons_agg.lust.item() * 100

    df_tot_housed_peons = pd.read_sql(f'''
        select tot_cap
        from game.vw_player_tot_cap
        where userid = {current_user.id}
        and gameid = {current_user.gameid}
        ''', db_engine)
    
    tot_house_cap = df_tot_housed_peons.tot_cap.item()


    cus_style = pygal.style.styles['dark']( 
                                        label_font_size=20,
                                        major_label_font_size=24,
                                        value_font_size=35,
                                        value_label_font_size=20,
                                        tooltip_font_size=35,
                                        title_font_size=24,
                                        legend_font_size=20,
                                        no_data_font_size=20,
                                        plot_background='rgba(34, 34, 34, 1)',
                                        background='rgba(34, 34, 34, 1)')

    pygal_kwargs = {
        'tooltip_border_radius': 10,
        'show_legend': False, 
        'style': cus_style
    }
    percent_formatter = lambda x: '{:.10g}%'.format(x)

    line_chart = pygal.Bar(range=(0,100), **pygal_kwargs)
    line_chart.title = 'Peon Information'
    line_chart.x_labels = ['Happyness', 'Hunger', 'Thirst', 'Energy', 'Lust']
    line_chart.add('Average',  [happy_val, hunger_val, thirst_val, energy_val, lust_val])
    line_chart.value_formatter = percent_formatter
    line_data = line_chart.render_data_uri()

    gauge = pygal.Pie(half_pie=True, **pygal_kwargs)
    gauge.add('Peon Count', peon_count)
    if tot_house_cap > peon_count:
        spare_housing = tot_house_cap - peon_count
        gauge.add('Available Housing', spare_housing)
    elif tot_house_cap < peon_count:
        homeless = peon_count - tot_house_cap
        gauge.add('Homeless Peons', homeless)
    gauge.title = 'Peon Count vs Housing Capacity'
    gauge_data = gauge.render_data_uri()

    df_tot_fams = pd.read_sql(f'''
        select [Count] from game.vw_player_family_count
        where userid = {current_user.id}
        and gameid = {current_user.gameid} 
        ''', db_engine)
    tot_fams = df_tot_fams.Count.item()

    df_peon_gender = pd.read_sql(f'''
        select Gender, peon_count as 'Count' 
        from game.vw_player_peon_gender 
        where userid = {current_user.id} 
        and gameid = {current_user.gameid} 
        ''', db_engine)

    df_peon_so = pd.read_sql(f'''
        select Orientation, peon_count as 'Count' 
        from game.vw_player_peon_so 
        where userid = {current_user.id} 
        and gameid = {current_user.gameid} 
        ''', db_engine)
    tables = [peon_table]

    return render_template('overview_peons.html',  
                        tables=tables,
                        line_data=line_data,
                        gauge_data=gauge_data,
                        tot_fams=tot_fams,
                        df_peon_gender=df_peon_gender,
                        df_peon_so=df_peon_so)