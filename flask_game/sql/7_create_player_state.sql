go
use TERRARUM

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_player_state')
begin
drop table dbo.game_player_state
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_player_state_res') --resources
begin
drop table dbo.game_player_state_res
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_player_state_con') --construction
begin
drop table dbo.game_player_state_con
end

go
begin
create table game_player_state (
id int identity(1,1) primary key,
gmuid int not null, --gmu = game_maps_users
realm_pop int default 150,
constraint FK_PS_MUID FOREIGN KEY (gmuid) REFERENCES game_maps_users(id),
)
end

go
begin
create table game_player_state_res ( --resources
id int identity(1,1) primary key,
stateid int not null,
resid int not null,
amount float not null default 0,
workers_assigned int not null default 0,
constraint FK_PSR_PSID FOREIGN KEY (stateid) REFERENCES game_player_state(id),
constraint FK_PSR_RID FOREIGN KEY (resid) REFERENCES game_resources(id),
)
end

go
begin
create table game_player_state_con ( --construction
id int identity(1,1) primary key,
stateid int not null,
buildid int not null,
amount float not null default 0,
workers_assigned int not null default 0,
constraint FK_PSC_PSID FOREIGN KEY (stateid) REFERENCES game_player_state(id),
constraint FK_PSC_BID FOREIGN KEY (buildid) REFERENCES game_buildings(id),
)
end
