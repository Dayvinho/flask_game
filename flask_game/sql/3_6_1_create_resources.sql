go
use TERRARUM


go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_res_types')
begin
drop table dbo.game_res_types
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_res_biomes')
begin
drop table dbo.game_res_biomes
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_resources')
begin
drop table dbo.game_resources
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_res_actions')
begin
drop table dbo.game_res_actions
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'game_tile_res')
begin
drop table dbo.game_tile_res
end

go
if exists(select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'res_nutrition')
begin
drop table dbo.res_nutrition
end


go
begin
create table res_types (
id int primary key,
res_type nvarchar(50) not null unique,
res_unit_type nvarchar(50) not null,
)
end

go
begin
create table resources (
id int identity(1,1) primary key,
res_name nvarchar(50) not null unique,
res_desc nvarchar(max),
res_typeid int not null,
constraint FK_R_RTID FOREIGN KEY (res_typeid) REFERENCES res_types(id),
)
end

go
BEGIN
create table res_nutrition ( -- per kg
id int identity(1,1) primary key,
resid int not null, 
carbs float not null,
protein float not null,
fiber float not null,
calories float not null,
fat float not null,
constraint FK_RN_RID FOREIGN KEY (resid) REFERENCES resources(id),
)
end

go
BEGIN -- when vegetables/fruits are in season
create table res_month_cycles (
id int identity(1,1) primary key,
resid int not null,
monthid int not null,
constraint FK_RMC_RID FOREIGN KEY (resid) REFERENCES resources(id),
constraint FK_RMC_MID FOREIGN KEY (monthid) REFERENCES months(monthid),
)
END


go
begin
create table res_actions (
id int identity(1,1) primary key,
res_action nvarchar(25) unique not null,
res_action_desc nvarchar(max), 
)
end

go
begin
create table res_biomes (
id int identity(1,1) primary key,
resid int not null, 
biomeid int not null,
spawn_chance float not null,  -- 0 -> 1.0 scale
min_spawn_amount float not null,
max_spawn_amount float not null,
constraint FK_RB_RID FOREIGN KEY (resid) REFERENCES resources(id),
constraint FK_RB_BID FOREIGN KEY (biomeid) REFERENCES biomes(id),
)
end

go
begin
create table game_map_tile_res (
id int identity(1,1) primary key,
resid int not null, 
tileid int not null,
amount int not null,
constraint FK_GTR_GMTID FOREIGN KEY (tileid) REFERENCES game_map_data(id),
constraint FK_GTR_RID FOREIGN KEY (resid) REFERENCES resources(id),

)
end


go
BEGIN
create table instruments (
id int identity(1,1) primary key,
instrument nvarchar(30) unique not null,
resid int not null,
constraint FK_I_RID FOREIGN KEY (resid) REFERENCES resources(id),
)
end

GO
begin
create table game_player_res (
id int identity(1,1) primary key,
userid int not null,
gameid int not null,
resid int not null,
amount float not null,
constraint FK_GPR_RID2 FOREIGN KEY (resid) REFERENCES resources(id),
constraint FK_GPR_UID2 FOREIGN KEY (userid) REFERENCES users_tbl(id),
constraint FK_GPR_GID2 FOREIGN KEY (gameid) REFERENCES game_maps(id),
)
end
;